import React from 'react';
import { Redirect } from 'react-router-dom';
import gql from 'graphql-tag';

import './css/Content.css';
import './css/CheckoutView.css';
import './css/Loader.css';
import './css/Snackbar.css';

import ProductList from './product/ProductList.js';
import ModalInput from './layoutComps/ModalInput.js';

class CheckoutView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: ["loading"],
      amounts: [],
      total: "",
      toggle: false,
      name: "",
      street: "",
      number: "",
      city: "",
      state: "",
      zip: "",
      country: "",
      countryList: [],
      stateList: [],
      response: "",
      redirect: false,
      spinner: false
    };

    this.updateAddressToggle = this.updateAddressToggle.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentDidMount() {
    document.title = "Bestellungsaufgabe | Skytala Bookstore";

    if (this.props.items !== undefined && this.props.items !== null) {
      var products = [];

      var tempPrs = this.props.items;
      tempPrs.forEach(function (pr) {
        let product = {
          productName: "",
          productPrices: "",
          productId: "",
          author: "",
          publishingDate: "",
          publisher: "",
          ISBN: "",
          mediumImageUrl: ""
        };

        //re-write productAttrbute array to obj-attributes so I can use them better..
        product.productId = pr.product.productId;
        product.productId = pr.product.productId;
        product.productName = pr.product.productName;
        product.productPrices = pr.product.productPrices;
        product.mediumImageUrl = pr.product.mediumImageUrl;
        product.author = pr.product.author;
        product.publishingDate = pr.product.publishingDate;
        product.publisher = pr.product.publisher;
        product.ISBN = pr.product.ISBN;
        product.amount = pr.numberProducts;

        pr.product.productAttributes.forEach(function (attr) { product[attr.attrName] = attr.attrValue; }, this);
        products.push(product);
      }, this);

      this.setState({ items: products, total: this.props.total });

      if (this.props.items.length === 0) this.setState({ items: [], amounts: [] });
    } else this.setState({ items: [], amounts: [] });

    if (this.props.countryList !== undefined)
      this.setState({ countryList: this.props.countryList });

    global.client.query({
      query: gql`{
        country(geoId:"${this.props.loggedInPerson.countryGeoId}") {
          regions{
            geoName
            geoId
          }
        }
      }`
    }).then(response => {
      if (response.data !== undefined)
        this.setState({ stateList: response.data.country.regions });
    }).catch(reason => {
      console.error('Login: ', reason);
    }).then(() => { });

    //var tempItems = [];
    //var tempAmounts = [];
    if (this.props.items !== undefined && this.props.items !== null &&
      this.props.amounts !== undefined && this.props.amounts !== null &&
      this.props.total !== undefined && this.props.total !== null) {
      this.setState({item: this.props.items, amounts: this.props.amounts, total: this.props.total});

      /*this.props.items.forEach(item => {
        tempAmounts.push(item.numberProducts);

        global.client.query({
          query: gql`{
            product(productId:"${item.product.productId}") {
              productId
              productName
              description
              longDescription
              author{
                attrValue
              }
              price{
                price
              }
          }`})
          .then(response => {
            response = response.data.product;
            tempItems.push(response);
            this.setState({ items: tempItems, amounts: tempAmounts });
          });
      });*/
    }

    //move this to AppWrap
    /*global.request.get("/eCommerce/api/cart/total")
      .withCredentials()
      .end((err, res) => {
        if (err || !res.ok) { } else this.setState({ total: res.body });
      });*/

    if (this.props.loggedInPerson !== undefined && this.props.loggedInPerson !== null) {
      this.setState({
        name: this.props.loggedInPerson.toName,
        street: this.props.loggedInPerson.address1,
        number: this.props.loggedInPerson.houseNumberExt,
        city: this.props.loggedInPerson.city,
        zip: this.props.loggedInPerson.postalCode,
        country: this.props.loggedInPerson.countryGeoId,
        state: this.props.loggedInPerson.stateProvinceGeoId
      });
    }
  }

  updateAddressToggle(e) {
    (this.state.toggle === true) ? this.setState({ toggle: false }) : this.setState({ toggle: true });
  }

  submit() {
    if (!document.getElementById("terms").checked) {
      this.setState({ response: "AGB nicht akzeptiert." });
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function () {
        x.className = x.className.replace("show", "");
      }, 3000);
    } else {
      this.setState({ spinner: true });

      global.client.mutate({
        mutation: gql`mutation{
          finishOrder(contact:{
            toName:"${this.state.name}",
            address1:"${this.state.street}",
            houseNumberExt:"${this.state.number}",
            city:"${this.state.city}",
            postalCode:"${this.state.zip}",
            countryGeoId:"${this.state.country}",
            stateProvinceGeoId:"${this.state.state}"
          }) {
            orderId
          } 
        }`
      }).then(response => {
        if (this.props.refreshItems !== undefined)
          this.props.refreshItems();
        this.setState({ redirect: true, response: "Bestellung aufgegeben." });
      }).catch(reason => {
        this.setState({
          response: reason.toString()
        });
      }).then(() => {
        var x = document.getElementById("snackbar");
        x.className = "show";
        setTimeout(function () {
          x.className = x.className.replace("show", "");
        }, 3000);
        this.setState({ spinner: false });
      });
    }
  }

  updateCountryValue(e) {
    this.setState({ country: e.target.value });
    global.client.query({
      query: gql`{
        country(geoId:"${e.target.value}") {
          regions{
            geoName
            geoId
          }
        }
      }`
    }).then(response => {
      if (response.data !== undefined) this.setState({ stateList: response.data.country.regions });
    }).catch(reason => { console.error('Login: ', reason); }).then(() => { });
  }

  render() {
    var total = parseFloat(this.state.total.toString().replace(",", ".")).toFixed(2).toString().replace(".", ",");
    if (this.state.items.length <= 0 || this.state.items[0] === "loading") total = "—";

    var dropdownCountry = [];
    this.state.countryList.map(function (item, index) {
      return dropdownCountry.push( <option key={index} value={item.geoId}>{item.geoName}</option> );
    }, this);

    var dropdownState = [];
    this.state.stateList.map(function (item, index) {
      return dropdownState.push( <option key={index} value={item.geoId}>{item.geoName}</option> );
    }, this);

    var stateWrap = (dropdownState.length > 0) ? (<div>
      <span>Bundesland/Staat/Provence:</span>
      <select value={this.state.state} onChange={e => this.setState({ state: e.target.value })} required>
        {dropdownState}
      </select>
    </div>) : "";

    return (
      <div id="inner-wrap">
        {this.state.redirect && <Redirect to="/order" />}
        <div id="snackbar">{this.state.response}</div>
        <div id="checkout">
          <div id="addresses" className="column">
            <div id="billing-add" className="column fullwidth">
              <p><strong>Lieferadresse</strong></p>

              <ModalInput title={"Name"} id={"name"} value={this.state.name} onchange={e => this.setState({ name: e.target.value })} />
              <ModalInput title={"Straße"} id={"street"} value={this.state.street} onchange={e => this.setState({ street: e.target.value })} class={"street"} />
              <ModalInput title={""} id={"number"} value={this.state.number} onchange={e => this.setState({ number: e.target.value })} class={"number"} />
              <ModalInput title={"Postleitzahl"} id={"zip"} value={this.state.zip} onchange={e => this.setState({ zip: e.target.value })} />
              <ModalInput title={"Stadt"} id={"city"} value={this.state.city} onchange={e => this.setState({ city: e.target.value })} />

              <span>Land:</span>
              <select value={this.state.country} onChange={e => this.updateCountryValue(e)} required>
                {dropdownCountry}
              </select>
              {stateWrap}
            </div>
          </div>

          <div id="reviewCol" className="column">
            <div id="review" className="column">
              <p> <strong>Bestellübersicht</strong> </p>
              <ProductList items={this.state.items} amounts={this.state.amounts} view="checkout" />
              <div id="total-price"> {total} € </div>

              <label for="terms">
                <input type="checkbox" name="terms" id="terms" />
                Ich akzeptiere die <a href="#terms">AGB</a> <span class="checkmark"></span>
              </label>
            </div>
          </div>
        </div>

        <div id="place-order">{this.state.spinner && <div className="loader"></div>}{this.state.items[0] !== "loading" && <button type="submit" onClick={this.submit}>Bestellung aufgeben</button>}</div>
      </div>
    );
  }
}

export default CheckoutView;
