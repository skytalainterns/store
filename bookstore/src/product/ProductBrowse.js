﻿import React from 'react';
import ReactDOM from 'react-dom';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';

import '../css/Content.css';
import '../css/ProductBrowse.css';
import '../css/modal.css';
import '../css/index.css';
import '../css/Loader.css';

import CreateProduct from './CreateProduct';
import ProductList from './ProductList';

class ProductBrowse extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: ["loading"],
      search: "",
      genre: "",
      year: "",
      minPrice: "",
      maxPrice: "",
      genres: [],
      toggle: false,
      admin: false,
      loggedInPerson: ""
    };

    this.updateMinPriceValue = this.updateMinPriceValue.bind(this);
    this.updateMaxPriceValue = this.updateMaxPriceValue.bind(this);
    this.updateYearValue = this.updateYearValue.bind(this);
    this.sendFullQuery = this.sendFullQuery.bind(this);
    this.sendGenreQuery = this.sendGenreQuery.bind(this);
  }

  //gets URI and certain term (i.e. ?search=xxx), returns value (i.e. xxx)
  checkUrlSegment(uri, term) {
    if (uri.includes(term)) {
      var temp = uri.substring(uri.indexOf(term) + term.length + 1).replace("%20", " ");
      if (temp.indexOf("&") >= 0) temp = temp.substring(0, temp.indexOf("&"));
      return temp;
    }
    return "";
  }

  //pulls all products data
  sendFullQuery() {
    global.client.query({
      query: gql`{
          products {
            productId
            productName
            productAttributes{
              attrValue
              attrName
            }
            productPrices{
              price
            }
            mediumImageUrl
          }
        }`})
      .then(response => {
        var products = [];

        var tempPrs = response.data.products.map(n => n);
        tempPrs.forEach(function (pr) {
          let product = {
            productName: "",
            productPrices: "",
            productId: "",
            author: "",
            publishingDate: "",
            publisher: "",
            ISBN: "",
            mediumImageUrl: ""
          };

          //re-writes productAttrbute array to obj-attributes so it can be used better
          product.productId = pr.productId;
          product.productName = pr.productName;
          product.productPrices = pr.productPrices;
          product.mediumImageUrl = pr.mediumImageUrl;

          product.author = pr.author;
          product.publishingDate = pr.publishingDate;
          product.publisher = pr.publisher;
          product.ISBN = pr.ISBN;

          pr.productAttributes.forEach(function(attr) { product[attr.attrName] = attr.attrValue; }, this);
          products.push(product);
        }, this);

        this.setState({ items: products });
      }).catch(reason => { console.error(reason); });
  }

  //pulls products of cetain category
  sendGenreQuery(genre) {
    global.client.query({
      query: gql`{
          productCategory(productCategoryId:"${genre}") {
            productCategoryMembers {
              product {
                productId
                productName
                productAttributes{
                  attrValue
                  attrName
                }
                productPrices{
                  price
                }
                mediumImageUrl
              }
            }
          }
        }`}).then(response => {
          var products = [];

          var tempPrs = response.data.productCategory.productCategoryMembers.map(n => n);
          tempPrs.forEach(function (pr) {
            let product = {
              productName: "",
              productPrices: "",
              productId: "",
              author: "",
              publishingDate: "",
              publisher: "",
              ISBN: "",
              mediumImageUrl: ""
            };

            //re-write productAttrbute array to obj-attributes so they can be used better.
            product.productId = pr.product.productId;
            product.productId = pr.product.productId;
            product.productName = pr.product.productName;
            product.productPrices = pr.product.productPrices;
            product.mediumImageUrl = pr.product.mediumImageUrl;
            product.author = pr.product.author;
            product.publishingDate = pr.product.publishingDate;
            product.publisher = pr.product.publisher;
            product.ISBN = pr.product.ISBN;

            pr.product.productAttributes.forEach(function (attr) { product[attr.attrName] = attr.attrValue; }, this);
            products.push(product);
          }, this);

          this.setState({ items: products });
      }).catch(reason => { console.error(reason); });
  }

  componentDidMount() {
    if (this.props.loggedInPerson !== undefined && this.props.loggedInPerson !== null &&
      this.props.loggedInPerson !== "") {
      if (this.props.loggedInPerson.authorities !== undefined) {
        (this.props.loggedInPerson.authorities.includes("FULLADMIN")) ? this.setState({ admin: true }) :
          this.setState({ admin: false });
      }
    }

    //takes url and trims it down to parameters
    var s = window.location.search;
    this.setState({ search: this.checkUrlSegment(s, "search") });
    this.setState({ minPrice: this.checkUrlSegment(s, "min") });
    this.setState({ maxPrice: this.checkUrlSegment(s, "max") });
    this.setState({ year: this.checkUrlSegment(s, "year") });
    var tempGenre = this.checkUrlSegment(s, "genre");
    this.setState({ genre: tempGenre });

    document.title = "Produktsuche | Skytala Bookstore";

    //pulls genres
    global.client.query({
      query: gql`query{
        productCategories {
          productCategoryId
          categoryName
        }
      }`}).then(response => { this.setState({ genres: response.data.productCategories }); })
      .catch(reason => { console.error(reason); });

    if (tempGenre !== undefined && tempGenre !== null && tempGenre !== "" && tempGenre !== -1) {
      this.sendGenreQuery(tempGenre);
    } else this.sendFullQuery();
  }

  componentWillReceiveProps() {
    var s = window.location.search;
    this.setState({
      search: this.checkUrlSegment(s, "search"),
      minPrice: this.checkUrlSegment(s, "min"),
      maxPrice: this.checkUrlSegment(s, "max"),
      year: this.checkUrlSegment(s, "year"),
      genre: this.checkUrlSegment(s, "genre")
    });
  }

  showCreateProduct() {
    ReactDOM.render(<CreateProduct />, document.getElementById('modal-createProduct'));
    var createPrdkt = document.getElementById('modal-createProduct');
    createPrdkt.style.display = "block";
  }

  //changes url after searchbar / dropdown use
  refreshURL(term, e) {
    var g = (term === "genre") ? e.target.value : this.state.genre;
    var y = (term === "year")  ? e.target.value : this.state.year;
    var miP = (term === "min") ? e.target.value : this.state.minPrice;
    var maP = (term === "max") ? e.target.value : this.state.maxPrice;
    var s = this.state.search;
    var query = [];

    if (g > 0) query.push("genre=" + g);
    if (y > 0) query.push("year=" + y);
    if (miP.length > 0 && miP > 0) query.push("min=" + miP);
    if (maP.length > 0) query.push("max=" + maP);
    if (s.length > 0) query.push("search=" + s);

    var uri = "";
    for (var i = 0; i < query.length; i++) {
      (i === 0) ? uri += "?" + query[i] : uri += "&" + query[i];
    }

    if (g !== undefined && g !== null && g !== "" && g !== -1 && g !== "-1") {
      this.sendGenreQuery(g);
    } else this.sendFullQuery();
    this.props.history.push("/product" + uri);
  }

  updateMinPriceValue(e) {
    this.setState({ minPrice: e.target.value.toString().replace(".", ",") });
    this.refreshURL("min", e);
  }

  updateMaxPriceValue(e) {
    this.setState({ maxPrice: e.target.value.toString().replace(".", ",") });
    this.refreshURL("max", e);
  }

  updateYearValue(e) {
    this.setState({ year: e.target.value });
    this.refreshURL("year", e);
  }

  updateGenreValue(e) {
    this.setState({ genre: e.target.value });
    this.refreshURL("genre", e);
  }

  sortByProperty(objArray, prop, direction) {
    if (arguments.length < 2)
      throw new Error("ARRAY, AND OBJECT PROPERTY MINIMUM ARGUMENTS, OPTIONAL DIRECTION");
    if (!Array.isArray(objArray))
      throw new Error("FIRST ARGUMENT NOT AN ARRAY");
    const clone = objArray.slice(0);
    const direct = arguments.length > 2 ? arguments[2] : 1; //Default to ascending
    const propPath = (prop.constructor === Array) ? prop : prop.split(".");
    clone.sort(function (a, b) {
      for (let p in propPath) {
        if (a[propPath[p]] && b[propPath[p]]) {
          a = a[propPath[p]];
          b = b[propPath[p]];
        }
      }
      // convert numeric strings to integers
      a = a.match(/^\d+$/) ? + a : a;
      b = b.match(/^\d+$/) ? + b : b;
      return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
    });
    return clone;
  }

  render() {
    var listClass = (this.state.toggle) ? "compact" : "";

    const sortedGenres = (this.state.genres !== null) ? this.sortByProperty(this.state.genres, "categoryName", 1) : [];
    var genreOptions = [];
    sortedGenres.forEach(function (item, index) {
      genreOptions.push(<option key={index} value={item.productCategoryId}>{item.categoryName}</option>);
    }, this);

    if (genreOptions.length === 1) genreOptions = "";

    var years = [];
    this.state.items.forEach(function (element) {
      if (element !== null && element.publishingDate !== undefined && element.publishingDate !== null) {
        years.push(element.publishingDate);
      }
    }, this);
    years.sort();

    var yearsOptions = [];
    years.forEach(function (item, index) {
      var tempYear = new Date(item).getFullYear().toString();
      var alreadyInList = false;
      for (var i = 0; i < index; i++) {
        if (new Date(years[i]).getFullYear().toString() === tempYear) alreadyInList = true;
      }
      if (!alreadyInList) yearsOptions.push(<option key={index} value={tempYear}>{tempYear}</option>);
    }, this);

    if (yearsOptions.length === 0) yearsOptions = "";

    return (
      <div id="inner-wrap">
        <div id="filterSidebar">
          <div className="segment">
            <p><small>Filter nach Genre:</small></p>
            <select value={this.state.genre} onChange={e => this.updateGenreValue(e)}>
              <option value="-1"></option> {genreOptions} </select>
          </div>

          <div className="segment">
            <p><small>Filter nach Jahr:</small></p>
            <select value={this.state.year} onChange={e => this.updateYearValue(e)}>
              <option value="-1"></option> {yearsOptions} </select>
          </div>

          <div className="segment price">
            <p><small>Filter nach Preis (€):</small></p>
            <span>von</span>
            <input placeholder="0" value={this.state.minPrice} onChange={e => this.updateMinPriceValue(e)} />
            <span>bis</span>
            <input placeholder="∞" value={this.state.maxPrice} onChange={e => this.updateMaxPriceValue(e)} />
          </div>

          <div id="toggleDiv">
            <button id="toggleCompact" onClick={() => this.setState({ toggle: !this.state.toggle })}>Ansicht wechseln</button>
            {this.state.admin && (<button id="createProductBtn" onClick={this.showCreateProduct}>Produkt erstellen</button>)}
          </div>
        </div>
        <div id="product-list" className={listClass}>
          <ProductList genre={this.state.genre} search={this.state.search} items={this.state.items}
            admin={this.state.admin} minPrice={this.state.minPrice} maxPrice={this.state.maxPrice}
            year={this.state.year} genres={this.state.genres} />
        </div>
      </div>
    );
  }
}

export default withRouter(ProductBrowse);