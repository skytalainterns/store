import React from 'react';
import gql from 'graphql-tag';

import '../css/Content.css';
import '../css/OrderView.css';
import '../css/Loader.css';

import AdminOrder from './AdminOrder';

class AdminOrderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeCol: -1, openedOrder: -1, orderDetails: [] };
    this.render = this.render.bind(this);
    this.selectCol = this.selectCol.bind(this);
    this.toggleOrderAccordion = this.toggleOrderAccordion.bind(this);
  }

  selectCol(i) {
    this.setState({ activeCol: i, openedOrder: -1 });
    this.toggleOrderAccordion(this.state.openedOrder);
  }

  //pulls info to given order and renders the productList to the order (in a panel which gets toggled)
  toggleOrderAccordion(id) {
    if (this.state.openedOrder !== id) {
      document.getElementById("order" + id).classList.add("active");

      global.client.query({
        query: gql`{
          order(orderId:"${id}") {
            orderItems { 
              quantity
              product{
                productName
                productId
                productPrices{
                  price
                }
                mediumImageUrl
                productAttributes{
                  attrName
                  attrValue
                }
              }
            }
          }
        }`
      }).then(response => {
        if (response !== '') {
          var others = document.getElementsByClassName("panel");
          for (let i = 0; i < others.length; i++) others.item(i).style.maxHeight = "0px";
          var orderItems = response.data.order.orderItems.map(m => m.product);
          this.setState({ orderDetails: orderItems });
          var panel = document.getElementById("panel" + id);
          panel.style.maxHeight = "300px";
        }
      });

      this.setState({ openedOrder: id });
    } else if (this.state.openedOrder !== -1) {
      var panelOld = document.getElementById("panel" + this.state.openedOrder);
      panelOld.style.maxHeight = "0px";
      document.getElementById("order" + this.state.openedOrder).classList.remove("active");
      this.setState({ openedOrder: -1 });
    }
  }

  render() {
    // renders orderFilterBar and the orderList.

    if (this.props.orders !== null && this.props.orders.length > 0 && this.props.orders !== "" && this.props.orders !== "loading") {
      var colClass = ["col", "col", "col", "col", "col", "col sep", "col", "col", "col"];

      if(this.state.activeCol !== 5) {
        colClass[this.state.activeCol] = "col active" + this.state.activeCol;
      } else colClass[5] = "col sep active5";

      var active = this.state.activeCol;
      var acc = this.toggleOrderAccordion.bind(this);
      var orderDetails = this.state.orderDetails;

      return (
        <div id="state">
          <div onClick={() => this.selectCol(0)} className={colClass[0]}><p>Alle</p></div>
          <div onClick={() => this.selectCol(1)} className={colClass[1]}><p>Verarbeitung</p></div>
          <div onClick={() => this.selectCol(2)} className={colClass[2]}><p>Genehmigt</p></div>
          <div onClick={() => this.selectCol(3)} className={colClass[3]}><p>Bezahlt</p></div>
          <div onClick={() => this.selectCol(4)} className={colClass[4]}><p>Versandt</p></div>
          <div onClick={() => this.selectCol(5)} className={colClass[5]}><p>Abgeschlossen</p></div>
          <div onClick={() => this.selectCol(6)} className={colClass[6]}><p>Storniert</p></div>
          <div onClick={() => this.selectCol(7)} className={colClass[7]}><p>Pausiert</p></div>
          <div onClick={() => this.selectCol(8)} className={colClass[8]}><p>Abgelehnt</p></div>
          {this.props.orders.map(function (order, index) {
            return (<AdminOrder key={index} order={order} active={active} toggleOrderAccordion={acc}
              orderDetails={orderDetails} />);
          })}
        </div>
      );
    } else if (this.props.orders === "loading") {
      return (<div id="spinner-wrap"><div className="loader big"></div></div>);
    } else {
      return (<div id="sorry"><p><small><i>Die Bestellliste scheint leer zu sein.</i></small></p></div>);
    }
  }
}

export default AdminOrderList;