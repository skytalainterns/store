import React from 'react';
import { Link } from 'react-router-dom';

import '../css/Content.css';

class Footer extends React.Component {
  render() {
    return (
      <div id="footer">
        <div id="footer-list">
          <ul>
            <li><Link to="/faq">FAQ</Link></li>
            <li><Link to="/contact">Kontakt</Link></li>
            <li><Link to="/impressum">Impressum</Link></li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Footer;