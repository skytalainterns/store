import React from 'react';
import gql from 'graphql-tag';

import '../css/Promo.css';

class Promo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { promos: [] };
  }

  //sends promo request
  componentDidMount() {
    global.client.query({
      query: gql`{
          promos {
            productPromoId
						promoName
						promoText
						showToCustomer
          }
        }`
    }).then(response => {
      this.setState({ promos: response.data.promos });
    }).catch(reason => {console.error(reason); });
  }

  render() {
    //prints random promo
    if (this.state.promos !== null && this.state.promos.length > 0) {
      var i = Math.floor((Math.random() * this.state.promos.length));
      return (<div id="promo" className="parallax">
          <h1>{this.state.promos[i].promoName}</h1>
          <p>{this.state.promos[i].promoText}</p>
        </div>);
    } else return (null);
  }
}

export default Promo;