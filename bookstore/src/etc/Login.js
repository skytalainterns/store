import React from 'react';
import ReactDOM from 'react-dom';
import https from 'https';
import gql from 'graphql-tag';

import '../css/modal.css';
import '../css/Login.css';

import ModalInput from '../layoutComps/ModalInput.js';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { user: "admin", pw: "ofbiz", response: "", spinner: false };
    this.submit = this.submit.bind(this);
  }

  submit() {
    //checks imputfields and request login, then reloads page.
    if (this.state.user !== "" && this.state.pw !== "") {
      this.setState({ spinner: true });
      https.Agent({ rejectUnauthorized: false }); //? seems to be a old part when we used axios

      global.client.mutate({
        mutation: gql`mutation {
            login(username: "${this.state.user}", password: "${this.state.pw}") {
              message
              status
            }
          }`})
        .then(response => {
          if (response.data !== undefined) {
            if (response.data.login.status === "401") {
              this.setState({ response: "Benutzername oder Passwort falsch." });
            } else if (response.data.login.message.includes("disabled")) {
              this.setState({ response: "Benutzer noch nicht freigeschalten." });
            } else if (response.data.login.status === "200") {
              this.setState({ response: "" });
              this.close();
              var url = window.location.toString();
              if (this.props.refreshLogin !== undefined) this.props.refreshLogin();
              if (url.includes("/verify/")) {
              } else {
                ReactDOM.unmountComponentAtNode(document.getElementById('modal-login'));
                window.location.reload();
              }
            } else {
              this.setState({ response: "Unbekannter Fehler." });
            }
          }
        }).catch(reason => {
          console.error('Login: ', reason);
          this.setState({ response: "Unbekannter Fehler." });
        }).then(() => { this.setState({ spinner: false }); });
    }
  }

  submitOnEnter(e) { if (e.key === 'Enter') this.submit(); }
  
  close() { 
    document.getElementById('modal-login').style.display = "none";
    ReactDOM.unmountComponentAtNode(document.getElementById('modal-login')); 
  }

  render() {
    return (
      <div className="modal-content">
        <span id="login-close" className="close" onClick={this.close}>&times;</span>
        <p className="title"><strong>Login</strong></p>

        <ModalInput title={"Benutzername"} id={"login-username"} value={this.state.user} onchange={e => this.setState({ user: e.target.value })} class={"half"} onkeypress={e => this.submitOnEnter(e)} />

        <ModalInput title={"Passwort"} id={"login-password"} value={this.state.pw} onchange={e => this.setState({ pw: e.target.value })} class={"half"} onkeypress={e => this.submitOnEnter(e)} type={"password"} />

        <input id="login-submit" type="submit" value="Einloggen" onKeyPress={e => this.submitOnEnter(e)} onClick={this.submit} />
        {this.state.spinner && <div className="loader"></div>}
        <p><small>{this.state.response}</small></p>
      </div>
    );
  }
}

export default Login;
