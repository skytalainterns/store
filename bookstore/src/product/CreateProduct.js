import React from 'react';
import gql from 'graphql-tag';

import '../css/modal.css';
import '../css/Snackbar.css';

import InfoContent from './InfoContent.js';
import GenresSelectContent from './GenresSelectContent.js';
import ImageUploadContent from './ImageUploadContent.js';
import ImageSelectContent from './ImageSelectContent.js';

class CreateProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productName: "", author: "", publisher: "", publishingDate: "", genres: [], isbn: "", description: "", price: "", mediumImageUrl: "", responseID: "", response: "", img: [], currentPage: 0, edited: false, spinner: false, genreList: []
    };

    this.timeOut = this.timeOut.bind(this);
  }

  componentWillMount() {
    this.setState({
      responseID: "", productName: "", author: "", publisher: "", publishingDate: "", genres: "", isbn: "", price: "", description: "", mediumImageUrl: "", img: [], currentPage: 0, edited: false
    });
  }

  //queries categories (and gives it later to the genresselect child)
  componentDidMount() {
    global.client.query({
      query: gql`query{
        productCategories {
          productCategoryId
          categoryName
        }
      }`})
      .then(response => {
        this.setState({ genreList: response.data.productCategories });
      });
  }

  timeOut(x) {
    x.className = x.className.replace("show", "");
    this.setState({ response: "" });
  }

  changeCurrentPage(page) {
    //does nothing, is still there (because it gets passed to the children)
  }

  render() {
    if (this.state.response.toString().length > 0) {
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(() => this.timeOut(x), 3000);
    }

    switch (this.state.currentPage) {
      default:
      case 0: return (<InfoContent productName={this.state.productName} author={this.state.author} publisher={this.state.publisher} publishingDate={this.state.publishingDate} isbn={this.state.isbn} price={this.state.price} description={this.state.description} term="speichern" changeCurrentPage={this.changeCurrentPage.bind(this)} />);
      case 1: return (<GenresSelectContent genreList={this.state.genreList} updateGenreList={this.updateGenreList} genres={this.state.genres} term="speichern" changeCurrentPage={this.changeCurrentPage.bind(this)} />);
      case 2: return (<ImageUploadContent term="speichern" changeCurrentPage={this.changeCurrentPage.bind(this)} />);
      case 3: return (<ImageSelectContent img={this.state.img} productId={this.state.productId} term="speichern" changeCurrentPage={this.changeCurrentPage.bind(this)} />);
    }
  }
}

export default CreateProduct;