import React from 'react';
import gql from 'graphql-tag';

import '../css/Profile.css';
import '../css/modal.css';
import '../css/index.css';
import '../css/Loader.css';
import '../css/Snackbar.css';

import ModalInput from '../layoutComps/ModalInput.js';

class Profile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      response: "", admin: false, countryList: [],
      stateList: [], partyId: "", personalTitle: "",
      firstName: "", nickname: "", middleName: "",
      lastName: "", gender: "", birthDate: "",
      emailAddress: "", userLoginId: "", oldPassword: "",
      currentPassword: "", passwordRetype: "", toName: "",
      address1: "", houseNumberExt: "", city: "",
      postalCode: "", countryGeoId: "", stateProvinceGeoId: "",
      editMode: false, spinner: false, toggleAddress: false
    };

    this.updateFirstName = this.updateFirstName.bind(this);
    this.updateLastName = this.updateLastName.bind(this);
    this.updateCountryGeoId = this.updateCountryGeoId.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.checkAttribute = this.checkAttribute.bind(this);
    this.updateStatelist = this.updateStatelist.bind(this);
  }

  updateStatelist(country) {
    global.client.query({
      query: gql`{
        country(geoId:"${country}") {
          regions{
            geoName
            geoId
          }
        }
      }`
    }).then(response => {
      if (response.data !== undefined)
        this.setState({ stateList: response.data.country.regions });
    }).catch(reason => console.error('Login: ', reason)).then(() => { });
  }

  componentDidMount() {
    document.title = "Profile | Skytala Bookstore";

    if (this.props.loggedInPerson !== undefined && this.props.loggedInPerson !== null && this.props.loggedInPerson !== "") {
      if (this.props.loggedInPerson.authorities.includes("FULLADMIN")) this.setState({ admin: true });
      else this.setState({ admin: false });

      this.setState({
        partyId: this.props.loggedInPerson.partyId,
        personalTitle: this.props.loggedInPerson.personalTitle,
        firstName: this.props.loggedInPerson.firstName,
        nickname: this.props.loggedInPerson.nickname,
        middleName: this.props.loggedInPerson.middleName,
        lastName: this.props.loggedInPerson.lastName,
        gender: this.props.loggedInPerson.gender,
        emailAddress: this.props.loggedInPerson.emailAddress,
        userLoginId: this.props.loggedInPerson.userLoginId,
        toName: this.props.loggedInPerson.toName,
        address1: this.props.loggedInPerson.address1,
        houseNumberExt: this.props.loggedInPerson.houseNumberExt,
        city: this.props.loggedInPerson.city,
        postalCode: this.props.loggedInPerson.postalCode,
        countryGeoId: this.props.loggedInPerson.countryGeoId,
        stateProvinceGeoId: this.props.loggedInPerson.stateProvinceGeoId
      });

      if (this.props.loggedInPerson.toName !== undefined) {
        if ((this.props.loggedInPerson.toName !== null && this.props.loggedInPerson.toName.length > 0) || (this.props.loggedInPerson.address1 !== null && this.props.loggedInPerson.address1.length > 0) || (this.props.loggedInPerson.houseNumberExt !== null && this.props.loggedInPerson.houseNumberExt.length > 0) || (this.props.loggedInPerson.city !== null && this.props.loggedInPerson.city.length > 0) || (this.props.loggedInPerson.postalCode !== null && this.props.loggedInPerson.postalCode.length > 0) || (this.props.loggedInPerson.countryGeoId !== null && this.props.loggedInPerson.countryGeoId.length > 0) || (this.props.loggedInPerson.stateProvinceGeoId !== null && this.props.loggedInPerson.stateProvinceGeoId.length > 0)) { this.setState({ toggleAddress: true }); }
      }

      //re-writes pulled date from ISO to german format.
      var bd = this.props.loggedInPerson.birthDate;
      if (document.getElementById("bdInput").type.toString() !== "date") {
        if (bd !== null && bd !== undefined) {
          if (bd.includes("-")) {
            var split = bd.toString().split("-");
            bd = split[2] + "." + split[1] + "." + split[0];
          }
          this.setState({ birthDate: bd });
        }
      } else this.setState({ birthDate: bd });
    }

    if (this.props.countryList !== undefined)
      this.setState({ countryList: this.props.countryList });
    if (this.props.loggedInPerson !== null)
      this.updateStatelist(this.props.loggedInPerson.countryGeoId);
  }

  updateFirstName(e) {
    this.setState({ firstName: e.target.value, toName: (e.target.value + " " + this.state.lastName) });
  }

  updateLastName(e) {
    this.setState({ lastName: e.target.value, toName: (this.state.firstName + " " + e.target.value) });
  }

  updateCountryGeoId(e) {
    this.setState({ countryGeoId: e.target.value });
    this.updateStatelist(e.target.value);
  }

  isDate(input) {
    var status = false;
    if (!input || input.length <= 0) { } else {
      var result = new Date(input);
      if (result.toString() === 'Invalid Date') { } else status = true;
    }
    return status;
  }

  checkAttribute(v, string) {
    if (v === undefined) {
      this.setState({ response: string + " nicht ausgefüllt." }); return false;
    } else if (v === null) {
      this.setState({ response: string + " nicht ausgefüllt." }); return false;
    } else if (v.toString() === "") {
      this.setState({ response: string + " nicht ausgefüllt." }); return false;
    } else if (v.length === 0) {
      this.setState({ response: string + " nicht ausgefüllt." }); return false;
    } else if (v.toString() === "null") {
      this.setState({ response: string + " nicht ausgefüllt." }); return false;
    } else {
      this.setState({ response: "" }); return true;
    }
  }

  toggleEditMode(e) {
    var pwChange = true;
    var okay = true;

    // checks inputsfields for input then sends edit request
    if (!this.state.editMode) { this.setState({ editMode: !this.state.editMode }); }
    if (this.state.editMode) {
      if (!this.checkAttribute(this.state.firstName, "Vorname")) okay = false;
      else if (!this.checkAttribute(this.state.lastName, "Nachname")) okay = false;
      else if (!this.checkAttribute(this.state.gender, "Geschlecht")) okay = false;
      else if (!this.checkAttribute(this.state.birthDate, "Geburtsdatum")) okay = false;
      else if (!(this.state.oldPassword.length > 0 || this.state.currentPassword.length >= 8 || this.state.passwordRetype === this.state.currentPassword)) {
        this.setState({
          response: "Die Passwortänderung hat nicht funktioniert."
        });
      } else if (okay) {
        if (this.state.oldPassword.length <= 0) pwChange = false;
        else if (this.state.currentPassword.length < 8) pwChange = false;
        else if (this.state.passwordRetype.length <= 0) pwChange = false;

        var date = new Date();

        if (this.state.birthDate.toString().includes(".")) {
          var split = this.state.birthDate.toString().split(".");
          date = new Date(split[2] + "-" + (split[1]) + "-" + split[0]);
        } else {
          date = new Date(this.state.birthDate.toString());
        }

        if (this.isDate(date)) {
          date = date.toISOString();

          var mutationString = `mutation{
            updateUserDetails(partyId:"${this.state.partyId}", userDetails:{
              personalTitle: "${this.state.personalTitle}",
              firstName: "${this.state.firstName}",
              middleName: "${this.state.middleName}",
              nickname: "${this.state.nickname}",
              lastName:"${this.state.lastName}",
              gender: ${this.state.gender},
              birthDate:"${date}",
              userLoginId:"${this.state.userLoginId}"`;

          if (pwChange) {
            mutationString += `,
              oldPassword:"${this.state.oldPassword}",
              currentPassword:"${this.state.currentPassword}",
              passwordRetype:"${this.state.passwordRetype}"`;
          }
          
          if (this.state.toggleAddress) {
            if (!this.checkAttribute(this.state.toName, "Name/Firma"))
              okay = false;
            else if (!this.checkAttribute(this.state.address1, "Straße"))
              okay = false;
            else if (!this.checkAttribute(this.state.houseNumberExt, "Hausnummer"))
              okay = false;
            else if (!this.checkAttribute(this.state.city, "Stadt"))
              okay = false;
            else if (!this.checkAttribute(this.state.postalCode, "Postleitzahl"))
              okay = false;
            else if (!this.checkAttribute(this.state.countryGeoId, "Land"))
              okay = false;
            else if (okay) {
              mutationString = `,
                  toName:"${this.state.toName}",
                  address1:"${this.state.address1}",
                  houseNumberExt:"${this.state.houseNumberExt}",
                  city:"${this.state.city}",
                  postalCode:"${this.state.postalCode}",
                  countryGeoId:"${this.state.countryGeoId}",
                  stateProvinceGeoId:"${this.state.stateProvinceGeoId}"`;
            }
          }

          mutationString = `})
              }`;

          global.client.mutate({
            mutation: gql(mutationString)
          }).then(response => {
            this.setState({
              response: "", oldPassword: "", currentPassword: "",
              passwordRetype: "", editMode: false
            });
            if (this.props.refreshLogin !== undefined) this.props.refreshLogin();
          }).catch(reason => {
            this.setState({ response: "Etwas schien nicht richtig zu funktionieren." });
            return false;
          });
        } else {
          this.setState({ response: "Das Datum scheint in einem unbekannten Format zu sein." });
        }
      }
    }

    if (this.state.response.toString().length > 0) {
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function () {
        x.className = x.className.replace("show", "");
      }, 3000);
      return false;
    }
  }

  render() {
    var toggleAddressClass = (!this.state.toggleAddress) ? "hide" : "";

    if (this.state.spinner) {
      return (<div id="inner-wrap"><div className="loader big"></div></div>);
    } else {
      var editMode = this.state.editMode;

      //renders country/state list depending on react-state.
      var dropdownCountry = [];
      dropdownCountry.push(
        <option value={""}></option>
      );

      if (this.state.countryList !== null) {
        this.state.countryList.map(function (item, index) {
          return dropdownCountry.push(<option key={index} value={item.geoId}>
            {item.geoName}
          </option>);
        }, this);
      }

      var dropdownState = [];
      dropdownState.push(
        <option value={""}></option>
      );
      this.state.stateList.map(function (item, index) {
        return dropdownState.push(<option key={index} value={item.geoId}>
          {item.geoName}
        </option>);
      }, this);

      var stateWrap = "";
      if (dropdownState.length > 0) {
        if (editMode) {
          stateWrap = (<div>
            <span>Bundesland/Staat/Provence: </span>
            <select value={this.state.stateProvinceGeoId} onChange={e => this.setState({ stateProvinceGeoId: e.target.value })} required>{dropdownState}</select>
          </div>);
        } else {
          stateWrap = (<div>
            <span>Bundesland/Staat/Provence:</span>
            <select value={this.state.stateProvinceGeoId} onChange={e => this.setState({ stateProvinceGeoId: e.target.value })} disabled>{dropdownState}</select>
          </div>);
        }
      }

      //checks and prints password checks
      var pwStrength = "";
      var pwStrClass = 0;
      if (this.state.currentPassword.length < 8) {
        pwStrength = (<span>zu kurz</span>);
        pwStrClass = 0;
      } else if (this.state.currentPassword.length === 8) {
        pwStrength = (<span><br />schwach</span>);
        pwStrClass = 1;
      } else if (this.state.currentPassword.length > 8 && this.state.currentPassword.length <= 10) {
        pwStrength = (<span><br />akzeptabel</span>);
        pwStrClass = 2;
      } else if (this.state.currentPassword.length > 10 && this.state.currentPassword.length <= 12) {
        pwStrength = (<span><br />stark</span>);
        pwStrClass = 3;
      } else if (this.state.currentPassword.length > 12 && this.state.currentPassword.length <= 15) {
        pwStrength = (<span>sehr stark</span>);
        pwStrClass = 4;
      } else if (this.state.currentPassword.length > 15 && this.state.currentPassword.length <= 20) {
        pwStrength = (<span>super stark</span>);
        pwStrClass = 5;
      } else if (this.state.currentPassword.length > 20 && this.state.currentPassword.length <= 50) {
        pwStrength = (<span>über stark</span>);
        pwStrClass = 6;
      } else if (this.state.currentPassword.length > 50) {
        pwStrength = (<span>unmensch-<br />lich stark</span>);
        pwStrClass = 7;
      }

      var pwCompare = "";
      var pwCompClass = "";
      if (this.state.currentPassword.length < 8) {
        pwCompare = (null);
        pwCompClass = "";
      } else if (this.state.currentPassword !== this.state.passwordRetype) {
        pwCompare = (<span>×</span>);
        pwCompClass = "nope";
      } else {
        pwCompare = (<span>✓</span>);
        pwCompClass = "okay";
      }

      if (editMode) {
        return (
          <div id="inner-wrap">
            <div id="profileContent">
              {this.state.spinner && <div className="loader"></div>}
              <input className="toggleEditModeOn" type="submit" value="Speichern" onClick={e => this.toggleEditMode(e)} />
              <div className="column">
                <p><strong>Benutzerdetails</strong></p>
                <ModalInput title={"Titel"} id={"personalTitle"} value={this.state.personalTitle} onchange={e => this.setState({ personalTitle: e.target.value })} class={""} />
                <ModalInput title={"Vorname"} id={"firstName"} value={this.state.firstName} onchange={e => this.setState({ firstName: e.target.value })} class={""} />
                <ModalInput title={"Spitzname"} id={"nickname"} value={this.state.nickname} onchange={e => this.setState({ nickname: e.target.value })} class={""} />
                <ModalInput title={"Zweitname"} id={"nickname"} value={this.state.middleName} onchange={e => this.setState({ middleName: e.target.value })} class={""} />
                <ModalInput title={"Nachname"} id={"lastName"} value={this.state.lastName} onchange={e => this.setState({ lastName: e.target.value })} class={""} />

                <span>Geschlecht:</span>
                <select value={this.state.gender} onChange={e => this.setState({ gender: e.target.value })} required>
                  <option value=""></option>
                  <option value="true">Männlich</option>
                  <option value="false">Weiblich</option>
                </select>

                <ModalInput title={"Geburtsdatum"} id={"birthDate"} value={this.state.birthDate} onchange={e => this.setState({ birthDate: e.target.value })} class={""} />

                <span>E-Mail-Adresse:</span>
                <input value={this.state.emailAddress} type="email" name="emailAddress" onChange={e => this.setState({ emailAddress: e.target.value })} disabled />
              </div>
              <div className="column">
                <p><strong>Logindetails</strong></p>
                <span>Benutzername:</span>
                <input value={this.state.userLoginId} type="text" name="userLoginId" disabled />

                <ModalInput title={"Aktuelles Passwort"} id={"oldPassword"} value={this.state.oldPassword} onchange={e => this.setState({ oldPassword: e.target.value })} class={"pw"} type={"password"} />
                <ModalInput title={"Neues Passwort"} id={"currentPassword"} value={this.state.currentPassword} onchange={e => this.setState({ currentPassword: e.target.value })} class={"pw"} type={"password"} />
                <span id="pwStrength" className={"s" + pwStrClass}>{pwStrength}</span>
                <ModalInput title={"Password bestätigen"} id={"passwordRetype"} value={this.state.passwordRetype} onchange={e => this.setState({ passwordRetype: e.target.value })} class={"pw"} type={"password"} />
                <span id="pwComp" className={pwCompClass}>{pwCompare}</span>
              </div>
              <div className={"column address " + toggleAddressClass}>
                <label htmlFor="toggleAddress">
                  <input checked={this.state.toggleAddress} type="checkbox" name="toggleAddress" id="toggleAddress" onChange={e => this.setState({ toggleAddress: !this.state.toggleAddress })} />
                  <span class="checkmark"></span>
                  <p><strong>Standard-Adresse</strong></p>
                </label>

                <ModalInput title={"Name / Firma"} id={"toName"} value={this.state.toName} onchange={e => this.setState({ toName: e.target.value })} />
                <ModalInput title={"Straße"} id={"address1"} value={this.state.address1} onchange={e => this.setState({ address1: e.target.value })} class={"street"} />
                <ModalInput title={""} id={"houseNumberExt"} value={this.state.houseNumberExt} onchange={e => this.setState({ houseNumberExt: e.target.value })} class={"houseNumber"} />
                <ModalInput title={"Postleitzahl"} id={"postalCode"} value={this.state.postalCode} onchange={e => this.setState({ postalCode: e.target.value })} />
                <ModalInput title={"Stadt"} id={"city"} value={this.state.city} onchange={e => this.setState({ city: e.target.value })} />

                <span>Land:</span>
                <select value={this.state.countryGeoId} onChange={e => this.updateCountryGeoId(e)}>
                  {dropdownCountry}
                </select>
                {stateWrap}
              </div>
            </div>
            <div id="snackbar">{this.state.response}</div>
          </div>
        );
      } else {
        return (
          <div id="inner-wrap">
            <div id="profileContent">
              <input className="toggleEditModeOff" type="submit" value="&#9998; Bearbeiten" onClick={e => this.toggleEditMode(e)} />
              <div className="column">
                <p><strong>Benutzerdetails</strong></p>
                <span>Titel:</span>
                <input value={this.state.personalTitle} type="text" name="personalTitle" disabled />
                <span>Vorname:</span>
                <input value={this.state.firstName} type="text" name="firstName" disabled />
                <span>Spitzname:</span>
                <input value={this.state.nickname} type="text" name="nickname" disabled />
                <span>Zweitname:</span>
                <input value={this.state.middleName} type="text" name="middleName" disabled />
                <span>Nachname:</span>
                <input value={this.state.lastName} type="text" name="lastName" disabled />
                <span>Geschlecht:</span>
                <select value={this.state.gender} disabled>
                  <option value=""></option>
                  <option value="true">Männlich</option>
                  <option value="false">Weiblich</option>
                </select>
                <span>Geburtsdatum:</span>
                <input value={this.state.birthDate} type="date" id="bdInput" name="birthDate" disabled />
                <span>E-Mail-Adresse:</span>
                <input value={this.state.emailAddress} type="email" name="emailAddress" disabled />
              </div>
              <div className="column">
                <p><strong>Logindetails</strong></p>
                <span>Benutzername:</span>
                <input value={this.state.userLoginId} type="text" name="userLoginId" disabled />
              </div>
              <div className={"column address " + toggleAddressClass}>
                <label><p><strong>Standard-Adresse</strong></p></label>
                <span>Name / Firma:</span>
                <input value={this.state.toName} type="text" name="toName" disabled />
                <span>Straße:</span>
                <input value={this.state.address1} className="street" type="text" name="address1" disabled />
                <input value={this.state.houseNumberExt} className="houseNumber" type="text" name="houseNumber" disabled />
                <span>Postleitzahl:</span>
                <input value={this.state.postalCode} type="text" name="postalCode" disabled />
                <span>Stadt:</span>
                <input value={this.state.city} type="text" name="city" disabled />
                <span>Land:</span>
                <select value={this.state.countryGeoId} disabled>
                  {dropdownCountry}
                </select>
                {stateWrap}
              </div>
            </div>
            <div id="snackbar"> {this.state.response} </div>
          </div>
        );
      }
    }
  }
}

export default Profile;
