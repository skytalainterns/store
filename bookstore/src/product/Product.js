import React from 'react';
import gql from 'graphql-tag';
import { Link, withRouter } from 'react-router-dom';

import '../css/Content.css';

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = { spinner: false, amount: this.props.amount };
    this.submitAmount = this.submitAmount.bind(this);
  }

  componentDidMount() { this.setState({ amount: this.props.item.amount }); }

  componentWillReceiveProps(nextProps) {
    if (nextProps.amount !== this.state.amount && this.state.amount === undefined)
      this.setState({ amount: nextProps.amount });
  }

  //calculates difference and changes product amount in cart
  submitAmount(e) {
    var temp = parseInt(this.props.item.amount, 10);
    var diff = temp - parseInt(this.state.amount, 10);
    if (diff > 0) {
      this.setState({ spinner: true });

      global.client.mutate({
        mutation: gql`mutation {
          removeFromCart(productId: "${this.props.item.productId}", count: ${diff})
        }`
      }).then(response => {
        if (this.props.refreshItems !== undefined) this.props.refreshItems();
      }).catch(reason => {
        console.err(reason);
      }).then(() => { this.setState({ spinner: false }); });
    } else if (diff < 0) {
      this.setState({ spinner: true });

      global.client.mutate({
        mutation: gql`mutation {
          addToCart(productId: "${this.props.item.productId}", count: ${Math.abs(diff)})
        }`
      }).then(response => {
        if (this.props.refreshItems !== undefined) this.props.refreshItems();
      }).catch(reason => {
      }).then(() => { this.setState({ spinner: false }); });
    }
  }

  render() {
    var title = this.props.item.productName;
    var amount = (this.props.item.amount === undefined) ? 0 : this.props.item.amount;

    var price = "";
    if (this.props.item.productPrices !== undefined) {
      price = this.props.item.productPrices[0].price.toString();
      if (price !== "") price = parseFloat(price.replace(",", ".")).toFixed(2).replace(".", ",");
    }

    var img = this.props.item.mediumImageUrl;
    if (img === undefined || img === "" || img == null) {
      var bc = "";
      if (this.props.item.productId !== undefined) {
        for (var i = 0; i < this.props.item.productId.length; i++)
          bc += this.props.item.productId.charCodeAt(i);
        bc = (bc % 16777215).toString(16);
      } else bc = "000000";
      img = "http://via.placeholder.com/475x600/" + bc + "/ffffff.png?text=" + title;
    } else img = global.baseURL + img;

    var priceDiv = (price !== "") ? (<span className="product-price"><small>{price.replace(".", ",")} €</small></span>) : "";

    priceDiv = (amount !== 0) ? (<div className="product-price"><div>
      {this.state.spinner && <div className="loader"></div>}
      <input className="amount" type="text" value={this.state.amount}
        onChange={e => this.setState({ amount: e.target.value })}></input>
      <span> <small>&times; {price} €</small> </span>
      <input className="change" type="submit" value="Ändern"
        onClick={e => this.submitAmount(e)}></input>
    </div></div>) : priceDiv;

    priceDiv = (this.props.view === "checkout") ? (<div className="product-price"><div>
      {this.state.spinner && <div className="loader"></div>}
      <input className="amount" type="text" value={this.state.amount} disabled></input>
      <span> <small>&times; {price} €</small> </span>
    </div></div>) : priceDiv;

    if (amount !== 0) {
      return (<div className="product">
        <Link to={"/product/" + this.props.item.productId}>
          <div className="product-image"><img src={img} alt="" /></div>
        </Link >
        <div className="product-description"> {priceDiv}
          <p><strong>{title}</strong></p>
          <p>von {this.props.item.author !== undefined && this.props.item.author}</p>
        </div>
      </div>);
    } else {
      return (<div className="product"><Link to={"/product/" + this.props.item.productId}>
        <div className="product-image"><img src={img} alt="" /></div>
        <div className="product-description"> {priceDiv}
          <p><strong>{title}</strong></p>
          <p>von {this.props.item.author !== undefined && this.props.item.author}</p>
        </div>
      </Link ></div>);
    }
  }
}

export default withRouter(Product);