import React from 'react';
import { Link, Redirect, withRouter } from 'react-router-dom';
import gql from 'graphql-tag';

import '../css/index.css';
import '../css/Content.css';
import '../css/Alert.css';

class Verification extends React.Component {
  constructor(props) {
    super(props);
    this.state = { response: "", hash: "", redirect: false };
    this.resend = this.resend.bind(this);
  }

  componentDidMount() {
    document.title = "Verifizierung | Skytala Bookstore";

    if (this.props.loggedInPerson !== undefined && this.props.loggedInPerson !== null) {
      if (this.props.loggedInPerson.partyId !== undefined) {
        this.setState({ redirect: true });
      }
    }

    //pulls hash key from URI and sends verify request. 
    var url = window.location.toString();
    var hash = url.substring(url.indexOf("/verify/") + 8);
    if (hash !== "") {
      global.client.query({
        query: gql`{
        verify(hash: "${hash}")
      }`}).then(response => {
          if (response.data !== undefined) {
            this.setState({ response: response.data });
          }
        }).catch(reason => {
          console.error('resendVerificationMail: ', reason);
          this.setState({ response: "Unbekannter Fehler." });
        }).then(() => { });
    }
    this.setState({ hash: hash });
  }

  //resends verification mail
  resend() {
    var hash = "";
    global.client.query({
      query: gql`{
        resendVerificationMail(hash: "${hash}")
      }`}).then(response => {
        if (response.data !== undefined) {
          this.setState({ response: response.data });
        }
      }).catch(reason => {
        console.error('resendVerificationMail: ', reason);
        this.setState({ response: "Unbekannter Fehler." });
      }).then(() => { });
  }

  render() {
    var innerComp = "";
    if (this.state.response === "") {
      innerComp = (<div id="spinner-wrap"><div className="loader big"></div></div>);
    } else {
      var txt = <p><strong>Link ungültig.</strong><br /> Wir konnten mit diesem Link nichts anfangen.</p>;
      var btn = <Link to="/"><button>Zum Hauptmenü</button></Link>;
      var alertClass = "alert alert-error";
      if (this.state.response === "ok") {
        txt = <p><strong>Du bist nun einsatzbereit</strong><br /> Dein Account wurde erfolgreich verifiziert.</p>;
        btn = <Link to="/"><button>Jetzt stöbern</button></Link>;
        alertClass = "alert alert-okay";
      } else if (this.state.response === "expired") {
        txt = <p><strong>Link abgelaufen.</strong><br /> Die Zeit dieses Links is abgelaufen.</p>;
        btn = <button onClick={this.resend}>Erneut senden</button>;
      }
      innerComp = (<div className={alertClass}> {txt} {btn} </div>)
    }
    if (this.state.redirect) return (<Redirect to="/" />);
    else return (<div id="inner-wrap">{innerComp}</div>);
  }
}

export default withRouter(Verification);