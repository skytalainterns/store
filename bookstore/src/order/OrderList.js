import React from 'react';

import '../css/Content.css';
import '../css/OrderView.css';
import '../css/Loader.css';

import Order from './Order';

class OrderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeCol: -1 };
  }

  //renders orderList (with filter bar)
  render() {
    if (this.props.orders !== null && this.props.orders.length > 0 && this.props.orders !== "" && this.props.orders !== "loading") {
      var colClass0 = "col";
      var colClass1 = "col";
      var colClass2 = "col";
      var colClass3 = "col";
      var colClass4 = "col";
      var colClass5 = "col sep";
      var colClass6 = "col";
      var colClass7 = "col";
      var colClass8 = "col";

      switch (this.state.activeCol) {
        default:
        case 0: colClass0 = "col active0"; break;
        case 1: colClass1 = "col active1"; break;
        case 2: colClass2 = "col active2"; break;
        case 3: colClass3 = "col active3"; break;
        case 4: colClass4 = "col active4"; break;
        case 5: colClass5 = "col sep active5"; break;
        case 6: colClass6 = "col active6"; break;
        case 7: colClass7 = "col active7"; break;
        case 8: colClass8 = "col active8"; break;
      }

      var active = this.state.activeCol;
      return (
        <div id="state">
          <div onClick={() => this.setState({ activeCol: 0 })} className={colClass0}><p>Alle</p></div>
          <div onClick={() => this.setState({ activeCol: 1 })} className={colClass1}><p>Verarbeitung</p></div>
          <div onClick={() => this.setState({ activeCol: 2 })} className={colClass2}><p>Genehmigt</p></div>
          <div onClick={() => this.setState({ activeCol: 3 })} className={colClass3}><p>Bezahlt</p></div>
          <div onClick={() => this.setState({ activeCol: 4 })} className={colClass4}><p>Versandt</p></div>
          <div onClick={() => this.setState({ activeCol: 5 })} className={colClass5}><p>Abgeschlossen</p></div>
          <div onClick={() => this.setState({ activeCol: 6 })} className={colClass6}><p>Storniert</p></div>
          <div onClick={() => this.setState({ activeCol: 7 })} className={colClass7}><p>Pausiert</p></div>
          <div onClick={() => this.setState({ activeCol: 8 })} className={colClass8}><p>Abgelehnt</p></div>
          {this.props.orders.map(function (order, index) {
            return <Order key={index} order={order} active={active} />;
          })}
        </div>
      );
    } else if (this.props.orders === "loading") {
      return (<div id="spinner-wrap"><div className="loader big"></div></div>);
    } else {
      return (<div id="sorry"><p><small><i>Die Bestellliste scheint leer zu sein.</i></small></p></div>);
    }
  }
}

export default OrderList;
