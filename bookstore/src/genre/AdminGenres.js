import React from 'react';
import gql from 'graphql-tag';

import '../css/modal.css';
import '../css/Snackbar.css';

import AdminGenre from './AdminGenre.js';

class AdminGenres extends React.Component {
  constructor(props) {
    super(props);
    this.state = { genres: [], edited: false, spinner: false };
    this.addGenre = this.addGenre.bind(this);
    this.deleteGenre = this.deleteGenre.bind(this);
  }

  componentWillMount() { this.setState({ genres: [], edited: false }); }
  componentDidMount() { this.setState({ genres: this.props.genres, edited: false }); }
  componentWillReceiveProps(nextProps) { this.setState({ genres: nextProps.genres, edited: false }); }
  close() { document.getElementById('modal-editGenres').style.display = "none"; }

  deleteGenre(id) {
    global.client.mutate({
      mutation: gql`mutation{
        deleteProductCategoryById(productCategoryId: "${id}") {
          body
          contentType
          status
        }
      }`
    }).then(response => {
      var tempGenres = this.state.genres;
      tempGenres.splice(tempGenres.indexOf(id), 1);
      this.setState({ genres: tempGenres });
    }).catch(reason => {
      console.error('AdminGenres - deleteProductCategory: ', reason);
    });
  }

  addGenre(e) {
    if (e.key === 'Enter' && e.target.value !== "") {
      this.setState({ spinner: true });
      var newGenre = {
        productCategoryId: "",
        categoryName: e.target.value,
        description: ""
      };

      global.client.mutate({
        mutation: gql`mutation{
          createProductCategory(productCategoryToBeAdded: {categoryName: "${e.target.value}"}) {
            categoryName
            description
            productCategoryId
          }
        }`
      }).then(response => {
        newGenre.productCategoryId = response.data.productCategoryId;
        var tempGenres = this.state.genres;
        tempGenres.push(newGenre);
        this.setState({ genres: tempGenres, newGenre: "" });
      }).catch(reason => {
        console.error('AdminGenres - addProductCategory: ', reason);
      }).then(() => { this.setState({ spinner: false }); });
    }
  }

  render() {
    var genres = this.state.genres, delGen = this.deleteGenre;
    return (<div className="modal-content">
      <span id="login-close" className="close" onClick={this.close}>&times;</span>
      <p className="title"><strong>Genres hinzufügen / bearbeiten</strong></p>
      {genres.map(function (item, index) {
        return <AdminGenre key={index} genre={item} deleteGenre={delGen} />;
      })}
      <input className="genre-input" type="text" placeholder="Neues Genre…" value={this.state.newGenre} 
        onKeyPress={e => this.addGenre(e)} onChange={e => this.setState({ newGenre: e.target.value })} />
      {this.state.spinner && <div className="loader"></div>}
    </div>);
  }
}

export default AdminGenres;