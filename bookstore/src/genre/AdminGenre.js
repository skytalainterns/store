import React from 'react';
import gql from 'graphql-tag';

import '../css/modal.css';

class AdminGenre extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      genresID: this.props.genre.productCategoryId,
      genresName: this.props.genre.categoryName,
      genresDesc: this.props.genre.description,
      spinner: false, edited: false
    };

    this.updateGenre = this.updateGenre.bind(this);
  }
  
  updateGenre(e, id) {
    if (e.key === 'Enter' && this.state.edited && this.state.genresName !== "" && this.state.genresName !== null && this.state.genresName !== undefined) {
      this.setState({ spinner: true });
      global.client.mutate({
        mutation: gql`mutation{
          updateProductCategory(productCategoryId:"${this.state.genresID}", productCategoryToBeUpdated:{categoryName:"${this.state.genresName}"}) {
            categoryName
            description
            productCategoryId
          }
        }`
      }).then(response => { this.setState({ spinner: false }); }).catch(reason => {
        console.error('AdminGenres - editProductCategory: ', reason);
      }).then(() => { this.setState({ spinner: false }); });
    }
  }

  render() {
    return (
      <div>
        <input className="genre-input" type="text" value={this.state.genresName}
          onKeyPress={e => this.updateGenre(e, this.state.genresID)}
          onChange={e => this.setState({ genresName: e.target.value, edited: true })} />
        {this.state.spinner && <div className="loader"></div>}
        <input className="genre-delete" type="button"
          onClick={() => this.props.deleteGenre(this.state.genresID)} value="&times;" />
      </div>
    );
  }
}

export default AdminGenre;