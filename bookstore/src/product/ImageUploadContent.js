import React from 'react';
import ReactDOM from 'react-dom';
import gql from 'graphql-tag';

class ImageUploadContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.uploadImages = this.uploadImages.bind(this);
  }

  componentDidMount() {
    this.setState({productId: this.props.productId});
  }

  close() {
    document.getElementById('modal-editProduct').style.display = "none";
    ReactDOM.unmountComponentAtNode(document.getElementById('modal-editProduct'));
  }

  uploadImages() {
    this.setState({ response: "", spinner: true });

    var formData = new FormData();
    var imagefile = document.getElementById('Image');
    if (imagefile.files[0] !== undefined) formData.append("additionalImageOne", imagefile.files[0]);
    if (imagefile.files[1] !== undefined) formData.append("additionalImageTwo", imagefile.files[1]);
    if (imagefile.files[2] !== undefined) formData.append("additionalImageThree", imagefile.files[2]);
    if (imagefile.files[3] !== undefined) formData.append("additionalImageFour", imagefile.files[3]);
    if (imagefile.files[4] !== undefined) formData.append("additionalImageFive", imagefile.files[4]);
    if (imagefile.files[5] !== undefined) formData.append("additionalImageSix", imagefile.files[5]);
    if (imagefile.files[6] !== undefined) formData.append("additionalImageSeven", imagefile.files[6]);
    if (imagefile.files[7] !== undefined) formData.append("additionalImageEight", imagefile.files[7]);
    if (imagefile.files[8] !== undefined) formData.append("additionalImageNine", imagefile.files[8]);
    if (imagefile.files[9] !== undefined) formData.append("additionalImageTen", imagefile.files[9]);
    formData.append('productId', this.state.productId);

    if (imagefile.files[0] !== undefined) {
      /*THIS IS HOW IS USED TO DO THE UPLOAD WITH SUPERAGENT, which worked flawlessly. GraphQL apparently can't upload images (and if it can: how?). 
      
      global.request.post("/eCommerce/api/service/products/multipleUploadProductImages")
        .withCredentials().send(formData).end((err, res) => {
          this.setState({ spinner: false });
          if (err || !res.ok) {
            this.setState({ response: "Bilder konnten nicht hochgeladen werden.", spinner: false });
          } else {
            global.request.get("/eCommerce/api/resources/productImageURLs/" + this.state.productId)
              .withCredentials().end((err, res) => {
                if (err || !res.ok) {} else {
                  res = res.body;
                  this.setState({ img: res, mediumImageUrl: res[0], currentPage: 3, response: "Bilder hochgeladen." });
                }});}});*/


      //todo: the following sadly doesn't work...
      var mutStr = `mutation {
        multipleUploadProductImages(
          productId: "${this.state.productId}"`;

      if (imagefile.files[0] !== undefined) mutStr += `, _additionalImageOne_fileName: "${imagefile.files[0]}"`;
      if (imagefile.files[1] !== undefined) mutStr += `, _additionalImageTwo_fileName: "${imagefile.files[1]}"`;
      if (imagefile.files[2] !== undefined) mutStr += `, _additionalImageThree_fileName: "${imagefile.files[2]}"`;
      if (imagefile.files[3] !== undefined) mutStr += `, _additionalImageFour_fileName: "${imagefile.files[3]}"`;
      if (imagefile.files[4] !== undefined) mutStr += `, _additionalImageFive_fileName: "${imagefile.files[4]}"`;
      if (imagefile.files[5] !== undefined) mutStr += `, _additionalImageSix_fileName: "${imagefile.files[5]}"`;
      if (imagefile.files[6] !== undefined) mutStr += `, _additionalImageSeven_fileName: "${imagefile.files[6]}"`;
      if (imagefile.files[7] !== undefined) mutStr += `, _additionalImageEight_fileName: "${imagefile.files[7]}"`;
      if (imagefile.files[8] !== undefined) mutStr += `, _additionalImageNine_fileName: "${imagefile.files[8]}"`;
      if (imagefile.files[9] !== undefined) mutStr += `, _additionalImageTen_fileName: "${imagefile.files[9]}"`;

      mutStr += `) {
          body
          contentType
          status
        }
      }`;

      global.client.mutate({
        mutation: gql`${mutStr}`
      }).then(response => {
        this.setState({ items: response.data.products });
      }).catch(reason => {
        console.error("error while upload images: " + reason);
      }).then(() => {
        this.setState({ spinner: false });
      });
    }
  }

  render() {
    return (
      <div className="modal-content">
        <span id="login-close" className="close" onClick={this.close}>&times;</span>
        <p className="title"><strong>Produkt {this.props.term}</strong></p>

        <div id="createPrPages">
          <p onClick={() => this.props.changeCurrentPage(0)}>1. Infos</p>
          <p onClick={() => this.props.changeCurrentPage(1)}>2. Genres</p>
          <p className="active">3. Bilder</p>
          <p onClick={() => this.props.changeCurrentPage(3)}>4. Cover</p>
        </div>

        <p><small>Laden Sie bis zu zehn Bilder für das Buch gleichzeitig hoch (zusammen max. 10 MB):</small></p>

        <div className="addProductImageSelect">
          <input id="Image" type="file" name="additionalImageOne" multiple />
        </div>

        {this.state.spinner && <div className="loader"></div>}
        <input id="createProduct-submit" type="submit" value="Hochladen & Weiter" onClick={() => this.uploadImages()} />
        <div id="snackbar">{this.state.response}</div>
      </div>
    );
  }
}

export default ImageUploadContent;