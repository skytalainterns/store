import React from 'react';
import gql from 'graphql-tag';

import '../css/Content.css';
import '../css/OrderView.css';
import '../css/Loader.css';

import OrderList from './OrderList';

class OrderView extends React.Component {
  constructor(props) {
    super(props);
    this.state = { orders: [], total: "", data: "" };
  }

  //calls orders data nd give it to the orderList.
  componentDidMount() {
    document.title = "Bestellliste | Skytala Bookstore";

    global.client.query({
      query: gql`{
        orders {
          orderId
          grandTotal
          statusId
          currencyUom
          orderDate
        }
      }`
    }).then(response => {
      this.setState({ orders: response.data.orders });
    });
  }

  render() {
    return (<div id="inner-wrap"><div id="order-list"><OrderList orders={this.state.orders} /></div></div>);
  }
}

export default OrderView;
