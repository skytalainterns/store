import React from 'react';
import { BrowserRouter, Route, Switch, Link, Redirect } from 'react-router-dom';
import gql from 'graphql-tag';

import './css/index.css';
import './css/Content.css';

import NavBar from './etc/NavBar';
import Promo from './etc/Promo';
import DetailView from './product/DetailView';
import ProductBrowse from './product/ProductBrowse';
import CartView from './CartView';
import CheckoutView from './CheckoutView';
import Footer from './etc/Footer';
import OrderView from './order/OrderView';
import OrderDetailView from './order/OrderDetailView';
import Profile from './etc/Profile';
import Register from './etc/Register';
import Genres from './genre/Genres';
import Verification from './etc/Verification';
import AdminOrderView from './order/AdminOrderView';
import CheckYourMails from './etc/CheckYourMails';

class AppWrap extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			response: "",
			loggedInPerson: "",
			cartNumber: 0,
			items: [],
			amounts: [],
			total: 0,
			countryList: []
		};

		this.refreshCartNumber = this.refreshCartNumber.bind(this);
		this.refreshItems = this.refreshItems.bind(this);
		this.refreshLogin = this.refreshLogin.bind(this);
	}

	componentDidMount() {
		this.refreshCartNumber(0);
		this.refreshItems();
		this.refreshLogin();

		global.client.query({
			query: gql`{
				countries {
					geoName
					geoId
				}
			}`
		}).then(response => { this.setState({ countryList: response.data.countries }); });
	}

	refreshCartNumber(amount) {
		this.setState({ cartNumber: (parseInt(this.state.cartNumber, 10) + parseInt(amount, 10)) });
	}

	refreshItems(amount) {
		global.client.query({
			query: gql`{
				cart{
					total
					positions{
						numberProducts
						product{
							productId
							productName
							productPrices{
								price
							}
							productAttributes{
								attrName
								attrValue
							}
							mediumImageUrl
						}
					}
				}
			}`
		}).then(response => { 
			let count = 0;
			response.data.cart.positions.forEach(function(element) {
				count += element.numberProducts;
			}, this);

			this.setState({ items: response.data.cart.positions, cartNumber: count, total: response.data.cart.total});
		});
	}

	refreshLogin() {
		global.client.query({
			query: gql`{
					loggedInPerson{
						personalTitle
						firstName
						nickname
						middleName
						lastName
						gender
						birthDate
						emailAddress

						userLoginId
						oldPassword
						currentPassword
						passwordRetype

						toName
						address1
						houseNumberExt
						postalCode
						city
						countryGeoId
						stateGeoId

						authorities
						partyId
					}
				}`
		}).then(response => { this.setState({ loggedInPerson: response.data.loggedInPerson }); });
	}

	render() {
		const PrimaryLayout = () => (
			<Switch>
				<Route exact path="/" component={main} />
				<Route exact path="/product" component={pBrowse} />
				<Route path="/product/:productId" component={pDetails} />
				<Route exact path="/cart" component={pCart} />
				<Route exact path="/checkout" component={pCheckout} />
				<Route exact path="/order" component={pOrder} />
				<Route path="/order/:orderId" component={pOrderDetail} />
				<Route exact path="/faq" component={pFaq} />
				<Route exact path="/contact" component={pContact} />
				<Route exact path="/impressum" component={pImpressum} />
				<Route exact path="/profile" component={pProfile} />
				<Route path="/profile/verify/:hash" component={pVerify} />
				<Route exact path="/checkYourMails" component={pCheck} />
				<Route exact path="/admin/order" component={pAdminOrderView} />
				<Route exact path="/register" component={pRegister} />
				<Route exact path="/genres" component={pGenres} />
				<Redirect to="/" />
			</Switch>
		);

		// MAIN PAGE
		const main = () => (
			<div style={{ minHeight: 'calc(100% - 224px)' }}>
				<div id="inner-wrap">
					<Promo />
					<div id="static-categories">
						<Link to="/product"><div className="cate" style={{ backgroundColor: "#261288" }}><p>Alle Bücher</p></div></Link>
						<Link to={"/genres"}><div className="cate" style={{ backgroundColor: "#130644" }}><p>Alle Genres</p></div></Link>
					</div>
				</div>
			</div>
		);

		const pDetails = ({ match }) => (<DetailView productId={match.params.productId} loggedInPerson={this.state.loggedInPerson} refreshCartNumber={this.refreshCartNumber} refreshItems={this.refreshItems} />);
		const pOrderDetail = ({ match }) => (<OrderDetailView orderId={match.params.orderId} />);
		const pBrowse = () => (<ProductBrowse loggedInPerson={this.state.loggedInPerson} />);
		const pCart = ({ match }) => (<CartView items={this.state.items} total={this.state.total} refreshCartNumber={this.refreshCartNumber} refreshItems={this.refreshItems} />);
		const pOrder = ({ match }) => (<OrderView />);
		const pCheckout = ({ match }) => (<CheckoutView loggedInPerson={this.state.loggedInPerson} items={this.state.items} total={this.state.total} refreshItems={this.refreshItems} countryList={this.state.countryList} />);

		// FOOTER LINKS
		const pFaq = ({ match }) => (
			<div style={{ height: 'calc(100% - 224px)' }}>
				<div id="faq">
					<h1>FAQ</h1><h2>Frage Eins?</h2><p>Antwort Eins.</p><h2>Frage Zwei?</h2><p>Antwort Zwei.</p><h2>Frage Drei?</h2><p>Antwort Drei.</p>
				</div>
			</div>
		);

		const pContact = ({ match }) => (
			<div style={{ height: 'calc(100% - 224px)' }}><div id="contact"><p><strong>Kontakt</strong></p></div></div>
		);

		const pImpressum = ({ match }) => (
			<div style={{ height: 'calc(100% - 224px)' }}>
				<div id="impressum">
					<p><strong>Impressum</strong></p>
					<p>Anbieter:<br /> Max Mustermann<br /> Musterstraße 1<br /> 80999 München </p>
					<p>Kontakt:<br /> Telefon: 089/1234567-8<br /> Telefax: 089/1234567-9<br /> E-Mail: mail@mustermann.de<br /> Website: www.mustermann.de </p>
					<p>Bei redaktionellen Inhalten:</p>
					<p>Verantwortlich nach § 55 Abs.2 RStV<br /> Max Mustermann<br /> Musterstraße 2<br /> 80999 München </p>
				</div>
			</div>
		);

		const pProfile = ({ match }) => (<Profile loggedInPerson={this.state.loggedInPerson} countryList={this.state.countryList} refreshLogin={this.refreshLogin} />)
		const pVerify = ({ match }) => (<Verification loggedInPerson={this.state.loggedInPerson} />)
		const pCheck = ({ match }) => (<CheckYourMails />)
		const pRegister = ({ match }) => (<Register countryList={this.state.countryList} />)
		const pGenres = ({ match }) => (<Genres loggedInPerson={this.state.loggedInPerson} />)
		const pAdminOrderView = ({ match }) => (<AdminOrderView />)

		const RouterWrap = ({ match }) => (
			<div style={{ height: 'calc(100%)' }}>
				<NavBar loggedInPerson={this.state.loggedInPerson} cartNumber={this.state.cartNumber} refreshLogin={this.refreshLogin} />
				<PrimaryLayout />
				<Footer />
			</div>
		)

		return (<BrowserRouter><RouterWrap /></BrowserRouter>);
	}
}

export default AppWrap;
