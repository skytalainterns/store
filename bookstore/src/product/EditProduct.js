import React from 'react';
import gql from 'graphql-tag';

import '../css/modal.css';
import '../css/Snackbar.css';

import InfoContent from './InfoContent.js';
import GenresSelectContent from './GenresSelectContent.js';
import ImageUploadContent from './ImageUploadContent.js';
import ImageSelectContent from './ImageSelectContent.js';

class EditProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productId: "", productName: "", author: "", publisher: "", publishingDate: "", genres: [], isbn: "", description: "", price: "", mediumImageUrl: "", response: "", img: [], currentPage: 0, edited: false, spinner: false, genreList: []
    };

    this.timeOut = this.timeOut.bind(this);
    this.changeCurrentPage = this.changeCurrentPage.bind(this);
  }

  componentWillMount() {
    this.setState({
      productId: "", productName: "", author: "", publisher: "", publishingDate: "", genres: "", isbn: "", price: "", description: "", mediumImageUrl: "", img: [], currentPage: 0, edited: false
    });
  }

  componentDidMount() {
    this.setState({
      productId: this.props.productId, productName: this.props.productName, author: this.props.author, publisher: this.props.publisher, genres: this.props.genres, isbn: this.props.isbn, price: parseFloat(this.props.price).toFixed(2).toString().replace(".", ","), description: this.props.longDescription, mediumImageUrl: this.props.mediumImageUrl, img: [], currentPage: 0, edited: false
    });

    var pd = this.props.publishingDate;
    if (pd !== null && pd !== undefined) {
      if (pd.includes("-")) {
        var split = pd.toString().split("T");
        var split2 = split[0].toString().split("-");
        pd = split2[2] + "." + split2[1] + "." + split2[0];
      }
    }
    this.setState({ publishingDate: pd });

    global.client.query({
      query: gql`query{
        productCategories {
          productCategoryId
          categoryName
        }
      }`}).then(response => {
        this.setState({ genreList: response.data.productCategories });
      });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ productId: nextProps.productId, productName: nextProps.productName, author: nextProps.author, publisher: nextProps.publisher });

    var pd = nextProps.publishingDate;
    if (pd !== null) {
      if (pd.includes("-")) {
        var split = pd.toString().split("T");
        var split2 = split[0].toString().split("-");
        pd = split2[2] + "." + split2[1] + "." + split2[0];
      }
    }
    this.setState({
      publishingDate: pd, isbn: nextProps.isbn, price: parseFloat(nextProps.price).toFixed(2).toString().replace(".", ","), description: nextProps.longDescription, mediumImageUrl: nextProps.mediumImageUrl, img: [], currentPage: 0, edited: false
    });
  }

  timeOut(x) {
    x.className = x.className.replace("show", "");
    this.setState({ response: "" });
  }

  changeCurrentPage(page){
    this.setState({currentPage: page});
  }

  render() {
    if (this.state.response.toString().length > 0) {
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(() => this.timeOut(x), 3000);
    }

    switch (this.state.currentPage){
      default:
      case 0: return (<InfoContent productId={this.state.productId} productName={this.state.productName} author={this.state.author} publisher={this.state.publisher} publishingDate={this.state.publishingDate} isbn={this.state.isbn} price={this.state.price} description={this.state.description} term="bearbeiten" changeCurrentPage={this.changeCurrentPage.bind(this)} />); 
      case 1: return (<GenresSelectContent productId={this.state.productId} genreList={this.state.genreList} updateGenreList={this.updateGenreList} genres={this.state.genres} term="bearbeiten" changeCurrentPage={this.changeCurrentPage.bind(this)} />); 
      case 2: return (<ImageUploadContent productId={this.state.productId} term="bearbeiten" changeCurrentPage={this.changeCurrentPage.bind(this)} />); 
      case 3: return (<ImageSelectContent img={this.state.img} productId={this.state.productId} term="bearbeiten" changeCurrentPage={this.changeCurrentPage.bind(this)} />); 
    }
  }
}

export default EditProduct;