import React from 'react';
import ReactDOM from 'react-dom';
import { Link, withRouter } from 'react-router-dom';
import gql from 'graphql-tag';

import '../css/Content.css';
import '../css/NavBar.css';

import Login from './Login';
import Dropdown from '../layoutComps/Dropdown.js';

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: [], cartNumber: 0, search: "", user: "", admin: false, refresh: false };
    this.logout = this.logout.bind(this);
    this.showLogin = this.showLogin.bind(this);
  }

  componentDidMount() {
    //trims the search parameter from the URI and saves it in a state
    var s = window.location.search;
    if (s.includes("search"))
      this.setState({ search: decodeURIComponent(s.substring(s.indexOf("search") + 7)) });

    //checks for user data and sets the name and adminState
    if (this.props.loggedInPerson !== undefined && this.props.loggedInPerson !== null &&
      this.props.loggedInPerson !== "") {
      if (this.props.loggedInPerson.authorities !== undefined) {
        if (this.props.loggedInPerson.authorities.includes("FULLADMIN")) this.setState({ admin: true });
      } else this.setState({ admin: false });
      this.setState({ user: this.props.loggedInPerson.firstName });
    }

    //sets the cartAmount
    if (this.props.cartNumber !== undefined && this.props.cartNumber !== null) {
      this.setState({ cartNumber: this.props.cartNumber });
    } else { this.setState({ cartNumber: 0 }); }
  }

  showLogin() {
    //renders and displays the login Modal.
    var refreshLogin = this.props.refreshLogin;
    ReactDOM.render(<Login refreshLogin={refreshLogin} />, document.getElementById('modal-login'));
    var modal = document.getElementById('modal-login');
    modal.style.display = "block";
  }

  submit(e) {
    //filters all parameters from the uri and re-links / re-writes the URI
    if (e.key === 'Enter') {
      var queryString = require('query-string');
      var parsed = queryString.parse(window.location.search);
      var g = (parsed.genre !== undefined) ? parsed.genre : "";
      var y = (parsed.year !== undefined) ? parsed.year : "";
      var miP = (parsed.min !== undefined) ? parsed.min : "";
      var maP = (parsed.max !== undefined) ? parsed.max : "";
      var s = (this.state.search !== undefined) ? encodeURIComponent(this.state.search) : "";
      var query = [];
      if (g.length > 0) query.push("genre=" + g);
      if (y.length > 0) query.push("year=" + y);
      if (miP.length > 0 && miP > 0) query.push("min=" + miP);
      if (maP.length > 0) query.push("max=" + maP);
      if (s.length > 0) query.push("search=" + s);

      var uri = "";
      for (var i = 0; i < query.length; i++) {
        if (i === 0) uri += "?" + query[i];
        else uri += "&" + query[i];
      }

      this.props.history.push("/product" + uri);
    }
  }

  logout() {
    //queries a logout request, then resets user data.
    global.client.mutate({
      mutation: gql`mutation {
          logout {
            status
          }
        }`
    }).then(response => {
      if (response.data !== null) {
        response = response.data;
        this.setState({ user: "" });
        if (this.props.refreshLogin !== undefined) this.props.refreshLogin();
        ReactDOM.unmountComponentAtNode(document.getElementById('modal-login'));
        window.location.reload();
      }
    });
  }

  render() {
    //renders links in dropout depending on adminState and name.
    var login = (this.state.user !== undefined && this.state.user.length > 0) ? "Hallo, " + this.state.user : "Login";
    var content = [{ type: "Link", to: "/register", text: "Registrieren" }];

    if (this.state.user !== undefined && this.state.user.length > 0) {
      if (!this.state.admin) {
        content = [{ type: "Link", to: "/profile", text: "Profil" },
        { type: "Link", to: "/order", text: "Meine Bestellungen" },
        { type: "span", id: "loginBtn", text: "Logout", onclick: this.logout }];
      } else {
        content = [{ type: "Link", to: "/profile", text: "Profil" },
        { type: "Link", to: "/admin/order", badge: (<span className='adminBadge'>Admin</span>), text: "Bestellstatus" },
        { type: "span", id: "loginBtn", text: "Logout", onclick: this.logout }];
      }
    }

    return (
      <div id="header">
        <Link to="/"><div id="logo">
          <img src="https://www.skytala-gmbh.com/assets/images/skytala-logo-white.png" alt="Skytala." />
        </div></Link>
        <Dropdown login={login} onclick={this.showLogin} content={content} />
        <div id="account-burger"><div></div><div></div><div></div></div>
        <div id="cart" className="dropdown"><Link to="/cart"><button className="dropbtn">
          <p>{this.state.cartNumber}</p>
          <img src="http://cdn1.crystalcommerce.com/themes/clients/categoryonegames/assets/img/ui/icon-cart.png" alt="" />
        </button></Link> </div>
        <div id="searchbar">
          <input placeholder="Suche…" type="text" value={this.state.search} onKeyPress={e => this.submit(e)} onChange={e => this.setState({ search: e.target.value })} />
        </div>
      </div>
    );
  }
}

export default withRouter(NavBar);