import React from 'react';
import ReactDOM from 'react-dom';
import gql from 'graphql-tag';

class ImageSelectContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mediumImageUrl: "",
      productId: "",
      img: "",
      spinner: false,
      response: ""
    };
    this.selectCover = this.selectCover.bind(this);
  }

  //pulls imageUrls, pulls mediumImageUrl from server, sets ImageUrl state.
  componentDidMount() {
    this.setState({ img: this.props.img, productId: this.props.productId });

    global.client.query({
      query: gql`{
        productImageURLs(productId: "${this.props.productId}")
      }`
    }).then(response => {
      response = response.data.productImageURLs;
      this.setState({ img: response });
      this.setState({ mediumImageUrl: response[0] });
    }).catch(reason => {
      
    }).then(() => {
      global.client.query({
        query: gql`{
        product(productId:"${this.props.productId}") {
          mediumImageUrl
        }
      }`})
      .then(response => {
        this.setState({ mediumImageUrl: response.data.product.mediumImageUrl });
      });
    });
  }

  close() {
    document.getElementById('modal-editProduct').style.display = "none";
    ReactDOM.unmountComponentAtNode(document.getElementById('modal-editProduct'));
  }

  //sets mediumImageURL to selected URL.
  selectCover() {
    this.setState({ response: "", spinner: true });

    if (this.state.mediumImageUrl !== undefined) {
      global.client.mutate({
        mutation: gql`mutation{
          updateProductWithDetails(productId: "${this.state.productId}", dto:{
            mediumImageUrl:"${this.state.mediumImageUrl}"
          })
        }`
      }).then(response => {
        this.close();
        ReactDOM.unmountComponentAtNode(document.getElementById('modal-editProduct'));
        this.setState({ response: "OK." });
        window.location.reload();
      }).catch(reason => {
        this.setState({ response: "Cover konnte nicht ausgewählt werden." });
      }).then(() => {
        this.setState({ spinner: false });
      });
    }
  }

  selectImage(i) {
    this.state.img.forEach(function (item, index) {
      if (index === i) this.setState({ mediumImageUrl: item });
    }, this);
  }

  render() {
    var imgDiv = [];
    var img = (this.state.img !== "") ? this.state.img : [];

    img.forEach(function (item, index) {
      if (this.state.mediumImageUrl === item) {
        imgDiv.push(<img className="image selected" onClick={() => this.selectImage(index)} src={global.baseURL + item} alt={"img" + index} />);
      } else {
        imgDiv.push(<img className="image" onClick={() => this.selectImage(index)} src={global.baseURL + item} alt={"img" + index} />);
      }
    }, this);

    return (<div className="modal-content">
      <span id="login-close" className="close" onClick={this.close}>&times;</span>
      <p className="title"><strong>Produkt {this.props.term}</strong></p>

      <div id="createPrPages">
        <p onClick={() => this.props.changeCurrentPage(0)}>1. Infos</p>
        <p onClick={() => this.props.changeCurrentPage(1)}>2. Genres</p>
        <p onClick={() => this.props.changeCurrentPage(2)}>3. Bilder</p>
        <p className="active">4. Cover</p>
      </div>

      <p><small>Wählen Sie das Cover des Buches aus:</small></p>
      <div id="imageDiv">{imgDiv}</div>

      {this.state.spinner && <div className="loader"></div>}
      <input id="createProduct-submit" type="submit" value="Fertig" onClick={this.selectCover} />
      <div id="snackbar">{this.state.response}</div>
    </div>);
  }
}

export default ImageSelectContent;