import React from 'react';
import { Link } from 'react-router-dom';

import '../css/Content.css';
import '../css/OrderView.css';
import '../css/Loader.css';

class Order extends React.Component {
  render() {
    var orderClass = "order " + this.props.order.statusId;
    var shipDate = new Date(this.props.order.orderDate);
    var shipDateString = ((shipDate.getDate() < 10) ? ("0" + shipDate.getDate()) + "." : shipDate.getDate() + ".") + (((shipDate.getMonth() + 1) < 10) ? ("0" + (shipDate.getMonth() + 1) + ".") : (shipDate.getMonth() + 1) + ".") + shipDate.getFullYear() + " " + ((shipDate.getHours() < 10) ? ("0" + shipDate.getHours()) + ":" : shipDate.getHours() + ":") + ((shipDate.getMinutes() < 10) ? ("0" + shipDate.getMinutes()) : shipDate.getMinutes());

    var show = false;
    switch (this.props.active) {
      default:
      case 0: show = true; break;
      case 1: if (this.props.order.statusId === "ORDER_PROCESSING") show = true; break;
      case 2: if (this.props.order.statusId === "ORDER_APPROVED") show = true; break;
      case 3: if (this.props.order.statusId === "ORDER_CREATED") show = true; break;
      case 4: if (this.props.order.statusId === "ORDER_SENT") show = true; break;
      case 5: if (this.props.order.statusId === "ORDER_COMPLETED") show = true; break;
      case 6: if (this.props.order.statusId === "ORDER_CANCELLED") show = true; break;
      case 7: if (this.props.order.statusId === "ORDER_HOLD") show = true; break;
      case 8: if (this.props.order.statusId === "ORDER_REJECTED") show = true; break;
    }

    if (show) {
      return (<div className={orderClass}>
        <Link className="wrap" to={"/order/" + this.props.order.orderId}>
          <div className="totalValue">
            <p>{this.props.order.grandTotal.toFixed(2).toString().replace(".", ",")} €</p>
          </div>
          <div className="shipDate"><p>vom {shipDateString}</p></div>
          <div className="orderId"><p>Bestellung #{this.props.order.orderId}</p></div>
        </Link>
      </div>);
    } else return (null);
  }
}

export default Order;
