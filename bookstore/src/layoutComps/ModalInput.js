import React from 'react';

class ModalInput extends React.Component {
  render() {
    var type = (this.props.type !== undefined) ? this.props.type : "text";
    var keypress = (this.props.onkeypress !== undefined) ? this.props.onkeypress : null;

    //taken given information and prints it
    return (
      <div>
        {this.props.title !== "" && <span className={this.props.class}>{this.props.title}:</span>}
        <input id={this.props.id} className={this.props.class} placeholder={this.props.title} type={type} value={this.props.value} onChange={this.props.onchange} onKeyPress={keypress} autoComplete="off" />
        <br />
      </div>
    );
  }
}

export default ModalInput;