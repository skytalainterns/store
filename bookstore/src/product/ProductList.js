import React from 'react';
import { withRouter } from 'react-router-dom';

import '../css/Content.css';

import Product from './Product';

class ProductList extends React.Component {
  //filters given items based on given data(year/price/genre/term), returns List.
  filterStuff() {
    var searchterm = (this.props.search !== undefined) ? decodeURIComponent(this.props.search.toLowerCase()) : "";
    searchterm = searchterm.replace(/[-[\]{}()\\|]/g, '\\$&');
    var re = (searchterm !== undefined && searchterm !== "" && searchterm !== null && searchterm.toString().length > 0) ? new RegExp("[" + searchterm + "]", 'g') : "";

    var v = this.props.view;

    var genreterm = (this.props.genre !== undefined) ? this.props.genre.toLowerCase() : "";
    var list = [];
    var admin = this.props.admin, l = this.props.items.length;
    var min = (this.props.minPrice !== undefined) ? parseFloat(this.props.minPrice.toString().replace(",", ".")) : "";
    var max = (this.props.maxPrice !== undefined) ? parseFloat(this.props.maxPrice.toString().replace(",", ".")) : "";
    var year = (this.props.year !== undefined) ? parseInt(this.props.year, 10) : "";

    if (this.props.items instanceof Array) {
      this.props.items.map(function (item, index) {
        var found = 0;

        if (searchterm === undefined || searchterm === "" || searchterm === null || searchterm.toString().length === 0) { found = 1; }

        if (item.productName !== undefined && item.productName !== null && searchterm !== "") {
          if (item.productName.toLowerCase().includes(searchterm)) {
            found += (1.5 * (item.productName.toLowerCase().match(re) || []).length);
          }
        }

        if (item.author !== undefined && item.author !== null && searchterm !== "") {
          if (item.author.toLowerCase().includes(searchterm)) {
            found += (1.25 * (item.author.toLowerCase().match(re) || []).length);
          }
        }

        //FILTER BY PRICE
        if (item.productPrices !== undefined && item.productPrices !== null && found > 0) {
          if (min !== "") {
            if (parseFloat(item.productPrices[0].price.toString().replace(",", ".")) < min) found = 0;
          }

          if (max !== "" && max !== "∞") {
            if (parseFloat(item.productPrices[0].price.toString().replace(",", ".")) > max) found = 0;
          }
        }

        //FILTER BY YEAR
        if (found > 0 && !isNaN(year) && year !== "" && year !== -1 && year !== undefined) {
          if (item.publishingDate !== undefined && item.publishingDate !== null) {
            var tempDate = new Date(item.publishingDate);
            if (parseInt(tempDate.getFullYear().toString(), 10) !== year) found = 0;
          }
          else { found = 0; }
        }

        //FILTER BY GENRE (via URL)
        if (item.genres !== undefined && item.genres !== null && genreterm !== undefined && genreterm !== null && genreterm.length > 0 && found > 0 && genreterm !== "-1") {
          var b = false;
          item.genres.forEach(function (element) {
            if (element.genreId === genreterm) b = true;
          }, this);
          if (!b) found = 0;
        }

        if (found > 0) list.push(<Product key={index} item={item} admin={admin} match={found} view={v} />);
        if (index === l - 1) list.sort(function (a, b) { return b.props.match - a.props.match });
        return (null);
      });
    }
    return list;
  }

  render() {
    //Attributes for CART VIEW / CHECKOUT VIEW
   //var refreshCartNumber = (this.props.refreshCartNumber !== undefined) ? this.props.refreshCartNumber.bind(this) : "";
    //var refreshItems = (this.props.refreshItems !== undefined) ? this.props.refreshItems.bind(this) : "";

    //The Rest
    var prList = this.filterStuff();
    if (prList.length > 0 && this.props.items !== "" && this.props.items[0] !== "loading") {
      return (<div>{prList}</div>);
    } else if (this.props.items[0] === "loading") {
      return (<div id="spinner-wrap"><div className="loader big"></div></div>);
    } else if (this.props.items[0] === "error") {
      return (<div id="sorry"><p><strong>Es scheint als ob ein Verbindungsproblem besteht :(</strong></p></div>);
    } else {
      return (<div id="sorry"><p><strong>Entschuldigung, wir scheinen nichts zu finden :(</strong></p></div>);
    }
  }
}

export default withRouter(ProductList);