import React from 'react';
import gql from 'graphql-tag';
import ProductList from '../product/ProductList';

import '../css/Content.css';
import '../css/OrderView.css';
import '../css/Loader.css';

class AdminOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeCol: -1, statusId: "", orderDate: "", orderId: "", grandTotal: 0 };

    this.render = this.render.bind(this);
    this.approve = this.approve.bind(this);
    this.reject = this.reject.bind(this);
    this.hold = this.hold.bind(this);
    this.create = this.create.bind(this);
    this.send = this.send.bind(this);
    this.complete = this.complete.bind(this);
  }

  componentDidMount(){
    this.setState({
      statusId: this.props.order.statusId,
      orderDate: this.props.order.orderDate,
      orderId: this.props.order.orderId,
      grandTotal: this.props.order.grandTotal
    });
  }

  approve(e, id) {
    global.client.mutate({
      mutation: gql`mutation {
        approveOrder(orderId: "${id}") {
          statusId
        }
      }`
    }).then(response => {
      this.setState({ statusId: "ORDER_APPROVED" });
    });
  }

  reject(e, id) {
    global.client.mutate({
      mutation: gql`mutation {
        rejectOrder(orderId: "${id}") {
          statusId
        }
      }`
    }).then(response => {
      this.setState({ statusId: "ORDER_REJECTED" });
    });
  }

  hold(e, id) {
    global.client.mutate({
      mutation: gql`mutation {
        holdOrder(orderId: "${id}") {
          statusId
        }
      }`
    }).then(response => {
      this.setState({ statusId: "ORDER_HOLD" });
    });
  }

  create(e, id) {
    global.client.mutate({
      mutation: gql`mutation{
          createOrder(orderId:"${id}") {
            statusId
          }
        }`
    }).then(response => {
      this.setState({ statusId: "ORDER_CREATED" });
    });
  }

  send(e, id) {
    global.client.mutate({
      mutation: gql`mutation{
          sendOrder(orderId:"${id}") {
            statusId
          }
        }`
    }).then(response => {
      this.setState({ statusId: "ORDER_SENT" });
    });
  }

  complete(e, id) {
    global.client.mutate({
      mutation: gql`mutation{
          completeOrder(orderId:"${id}") {
            statusId
          }
        }`
    }).then(response => {
      this.setState({ statusId: "ORDER_COMPLETED" });
    });
  }

  render() {
    var shipDate = new Date(parseInt(this.state.orderDate, 10));
    var shipDateString = ((shipDate.getDate() < 10) ? ("0" + shipDate.getDate()) + "." : shipDate.getDate() + ".") + (((shipDate.getMonth() + 1) < 10) ? ("0" + (shipDate.getMonth() + 1) + ".") : (shipDate.getMonth() + 1) + ".") + shipDate.getFullYear() + " " + ((shipDate.getHours() < 10) ? ("0" + shipDate.getHours()) + ":" : shipDate.getHours() + ":") + ((shipDate.getMinutes() < 10) ? ("0" + shipDate.getMinutes()) : shipDate.getMinutes());

    var show = false;
    switch (this.props.active) {
      default:
      case 0: show = true; break;
      case 1: if (this.state.statusId === "ORDER_PROCESSING") show = true; break;
      case 2: if (this.state.statusId === "ORDER_APPROVED") show = true; break;
      case 3: if (this.state.statusId === "ORDER_CREATED") show = true; break;
      case 4: if (this.state.statusId === "ORDER_SENT") show = true; break;
      case 5: if (this.state.statusId === "ORDER_COMPLETED") show = true; break;
      case 6: if (this.state.statusId === "ORDER_CANCELLED") show = true; break;
      case 7: if (this.state.statusId === "ORDER_HOLD") show = true; break;
      case 8: if (this.state.statusId === "ORDER_REJECTED") show = true; break;
    }

    var adminBtns = (<div className="adminAction"></div>);
    if (this.state.statusId === "ORDER_PROCESSING")
      adminBtns = (<div className="adminAction">
        <button className="btnApr" onClick={e => this.approve(e, this.state.orderId)}>Annehmen</button>
        <button className="btnRej" onClick={e => this.reject(e, this.state.orderId)}>Ablehnen</button>
        <button className="btnHol" onClick={e => this.hold(e, this.state.orderId)}>Pausieren</button>
      </div>);
    else if (this.state.statusId === "ORDER_APPROVED")
      adminBtns = (<div className="adminAction">
        <button className="btnCre" onClick={e => this.create(e, this.state.orderId)}>Zahlung bestätigen</button>
      </div>);
    else if (this.state.statusId === "ORDER_HOLD")
      adminBtns = (<div className="adminAction">
        <button className="btnApr" onClick={e => this.approve(e, this.state.orderId)}>Annehmen</button>
        <button className="btnRej" onClick={e => this.reject(e, this.state.orderId)}>Ablehnen</button>
      </div>);
    else if (this.state.statusId === "ORDER_CREATED")
      adminBtns = (<div className="adminAction">
        <button className="btnSen" onClick={e => this.send(e, this.state.orderId)}>Sendung bestätigen</button>
      </div>);
    else if (this.state.statusId === "ORDER_SENT")
      adminBtns = (<div className="adminAction">
        <button className="btnCom" onClick={e => this.complete(e, this.state.orderId)}>Ankunft bestätigen</button>
      </div>);

    if (show) {
      return (
        <div className={"order " + this.state.statusId}>
          <div className="accordion" id={"order" + this.state.orderId} onClick={e => this.props.toggleOrderAccordion(this.state.orderId)}>
            <div className="totalValue">
              <p>{this.state.grandTotal.toFixed(2).toString().replace(".", ",")} €</p>
            </div>
            <div className="shipDate"><p>vom {shipDateString}</p></div>
            <div className="orderId"><p>Bestellung #{this.state.orderId}</p></div>
          </div>
          {adminBtns}
          <div className="panel" id={"panel" + this.state.orderId}>
            <ProductList items={this.props.orderDetails !== undefined && this.props.orderDetails} />
          </div>
        </div>);
    } else return (null);
  }
}

export default AdminOrder;
