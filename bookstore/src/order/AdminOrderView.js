import React from 'react';
import gql from 'graphql-tag';

import '../css/Content.css';
import '../css/OrderView.css';
import '../css/Loader.css';

import AdminOrderList from './AdminOrderList';

class AdminOrderView extends React.Component {
  constructor(props) {
    super(props);
    this.state = { orders: "loading", total: "", data: "", response: "" };
  }

  componentDidMount() {
    document.title = "[Admin] Bestellliste | Skytala Bookstore";

    //queries orders and gives them to the adminOrderList.
    global.client.query({
      query: gql`{
        orders {
          orderId
          grandTotal
          statusId
          currencyUom
          orderDate
        }
      }`
    }).then(response => { this.setState({ orders: response.data.orders }); });
  }

  render() {
    return (<div id="inner-wrap"><div id="order-list" className="admin">
      <AdminOrderList orders={this.state.orders} />
    </div></div>);
  }
}
export default AdminOrderView;