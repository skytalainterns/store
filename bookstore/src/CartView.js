import React from 'react';
import { Link } from 'react-router-dom';

import './css/Content.css';
import './css/CartView.css';
import './css/Loader.css';

import ProductList from "./product/ProductList";

class CartView extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: ["loading"], amounts: [], total: "" };
  }

  componentDidMount() {
    document.title = "Einkaufswagen | Skytala Bookstore";

    if (this.props.items !== undefined && this.props.items !== null) {
      var products = [];

      var tempPrs = this.props.items;
      tempPrs.forEach(function (pr) {
        let product = {
          productName: "",
          productPrices: "",
          productId: "",
          author: "",
          publishingDate: "",
          publisher: "",
          ISBN: "",
          mediumImageUrl: ""
        };

        //re-write productAttrbute array to obj-attributes so I can use them better..
        product.productId = pr.product.productId;
        product.productId = pr.product.productId;
        product.productName = pr.product.productName;
        product.productPrices = pr.product.productPrices;
        product.mediumImageUrl = pr.product.mediumImageUrl;
        product.author = pr.product.author;
        product.publishingDate = pr.product.publishingDate;
        product.publisher = pr.product.publisher;
        product.ISBN = pr.product.ISBN;
        product.amount = pr.numberProducts;

        pr.product.productAttributes.forEach(function (attr) { product[attr.attrName] = attr.attrValue; }, this);
        products.push(product);
      }, this);

      this.setState({ items: products, total: this.props.total});

      if (this.props.items.length === 0) this.setState({ items: [], amounts: [] });
    } else this.setState({ items: [], amounts: [] });
  }

  render() {
    if (this.state.items[0] !== "" && this.state.items[0] !== "loading" && this.state.items.length > 0) {
      return (<div id="inner-wrap"><div>
          <div id="product-list" className="cart">
            <ProductList items={this.state.items} amount={this.state.amounts}
              refreshCartNumber={this.props.refreshCartNumber.bind(this)}
              refreshItems={this.props.refreshItems.bind(this)} />
          </div>
          <span id="total-price">{parseFloat(this.state.total.toString().replace(",", ".")).toFixed(2).toString().replace(".", ",")} €</span>
          <div id="check-out-wrap">
            <Link to="/checkout">
              <input type="submit" value="Zur Kasse" />
            </Link>
          </div>
        </div></div>);
    } else if (this.state.items[0] === "loading") {
      return (<div id="inner-wrap"><div id="spinner-wrap">
        <div className="loader big"></div></div></div>);
    } else {
      return (<div id="inner-wrap"><div id="sorry">
        <p><small><i>Der Warenkorb scheint leer zu sein.</i></small></p>
      </div></div>);
    }
  }
}

export default CartView;