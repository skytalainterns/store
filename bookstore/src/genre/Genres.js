import React from 'react';
import ReactDOM from 'react-dom';
import gql from 'graphql-tag';
import { Link } from 'react-router-dom';

import '../css/modal.css';
import '../css/index.css';
import '../css/Loader.css';
import '../css/Genre.css';

import AdminGenres from './AdminGenres';

class Genres extends React.Component {

  constructor(props) {
    super(props);
    this.state = { response: "", genres: [], spinner: true, admin: false };
    this.editGenres = this.editGenres.bind(this);
  }

  sortByProperty(objArray, prop, direction) {
    if (arguments.length < 2) throw new Error("ARRAY, AND OBJECT PROPERTY MINIMUM ARGUMENTS, OPTIONAL DIRECTION");
    if (!Array.isArray(objArray)) throw new Error("FIRST ARGUMENT NOT AN ARRAY");
    const clone = objArray.slice(0);
    const direct = arguments.length > 2 ? arguments[2] : 1; //Default to ascending
    const propPath = (prop.constructor === Array) ? prop : prop.split(".");
    clone.sort(function (a, b) {
      for (let p in propPath) {
        if (a[propPath[p]] && b[propPath[p]]) {
          a = a[propPath[p]];
          b = b[propPath[p]];
        }
      }
      a = a.match(/^\d+$/) ? + a : a;
      b = b.match(/^\d+$/) ? + b : b;
      return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
    });
    return clone;
  }

  //pulls categories/genres and sorts them by categoryname. (also sets adminstate)
  componentDidMount() {
    document.title = "Genres | Skytala Bookstore";

    if (this.props.loggedInPerson !== undefined && this.props.loggedInPerson !== null &&
      this.props.loggedInPerson !== "") {
      if (this.props.loggedInPerson.authorities.includes("FULLADMIN")) this.setState({ admin: true });
      else this.setState({ admin: false });
    }

    global.client.query({
      query: gql`query {
        productCategories {
          productCategoryId
          categoryName
        }
      }`}).then(response => {
        var g = this.sortByProperty(response.data.productCategories, "categoryName", 1);
        this.setState({ genres: g, spinner: false });
      });
  }

  //renders adminGenres modal.
  editGenres() {
    ReactDOM.render(
      <AdminGenres genres={this.state.genres} />, document.getElementById('modal-editGenres'));
    var editPrdkt = document.getElementById('modal-editGenres');
    editPrdkt.style.display = "block";
  }

  render() {
    var genres = (this.state.genres !== null) ? this.state.genres : [];
    
    if (this.state.genres === null) {
      return (<div id="inner-wrap">
        <div id="spinner-wrap"><div className="loader big"></div></div>
      </div>);
    } else {
      return (<div id="inner-wrap">
          <div id="genreList">
            <div id="modal-editGenres" class="modal"></div>
            {this.state.admin && (<div id="editGenresBtnDiv">
              <button id="editGenresBtn" onClick={this.editGenres}>Genres hinzufügen/bearbeiten</button>
            </div>)}
            {genres.map(function (item, index) {
              return <Genre key={index} genre={item} />;
            })}
          </div>
        </div>);
    }
  }
}

class Genre extends React.Component {
  render() {
    return (<Link to={"/product?genre=" + this.props.genre.productCategoryId}>
      <div className="genre"><p>{this.props.genre.categoryName}</p></div>
    </Link>);
  }
}

export default Genres;