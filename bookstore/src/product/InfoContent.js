import React from 'react';
import ReactDOM from 'react-dom';
import gql from 'graphql-tag';

import ModalInput from '../layoutComps/ModalInput.js';

class InfoContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productId: "", productName: "",  author: "",  publisher: "",  publishingDate: "",  isbn: "",  
      price: "",  description: "",  edited: false, spinner: "", response: ""
    };
    this.createEditProduct = this.createEditProduct.bind(this);
  }

  componentDidMount() {
    this.setState({
      productId: this.props.productId, productName: this.props.productName, author: this.props.author,
      publisher: this.props.publisher, publishingDate: this.props.publishingDate,
      isbn: this.props.isbn, price: this.props.price,
      description: this.props.description,
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      productId: nextProps.productId, productName: nextProps.productName, author: nextProps.author,
      publisher: nextProps.publisher, publishingDate: nextProps.publishingDate,
      isbn: nextProps.isbn, price: nextProps.price,
      description: nextProps.description,
    });
  }

  close() {
    document.getElementById('modal-editProduct').style.display = "none";
    ReactDOM.unmountComponentAtNode(document.getElementById('modal-editProduct'));

    document.getElementById('modal-createProduct').style.display = "none";
    ReactDOM.unmountComponentAtNode(document.getElementById('modal-createProduct'));
  }

  isDate(input) {
    var status = false;
    if (!input || input.length <= 0) {
      status = false;
    } else {
      var result = new Date(input);
      if (result.toString() === 'Invalid Date') status = false;
      else status = true;
    }
    return status;
  }

  //checks inputs and sends create/edit query. 
  createEditProduct(createOrEdit) {
    this.setState({ response: "" });
    if (!this.state.edited) {
      this.setState({ currentPage: 1 });
    } else if (this.state.edited) {
      if (this.state.productName == null || this.state.productName.length === 0) {
        this.setState({ response: "Titel muss gegeben sein." });
      } else if (this.state.author == null || this.state.author.length === 0) {
        this.setState({ response: "Autor muss gegeben sein." });
      } else if (this.state.publisher == null || this.state.publisher.length === 0) {
        this.setState({ response: "Verlag muss gegeben sein." });
      } else if (this.state.isbn == null || this.state.isbn.length === 0) {
        this.setState({ response: "ISBN muss gegeben sein." });
      } else if (this.state.price == null || this.state.price === "") {
        this.setState({ response: "Preis muss gegeben sein." });
      } else {
        //check date
        var publishingDate = new Date();

        if (this.state.publishingDate !== null) {
          if (this.state.publishingDate.toString().includes(".")) {
            var split = this.state.publishingDate.toString().split(".");
            publishingDate = new Date(split[2] + "-" + (split[1]) + "-" + split[0]);
          } else {
            publishingDate = new Date(this.state.publishingDate.toString());
          }
        }

        if (this.isDate(publishingDate)) {
          publishingDate = publishingDate.toISOString();
          this.setState({ spinner: true });

          let desc = this.state.description;
          desc = desc.replace(/%0A/g, "<br />");
          desc = desc.replace(/<br\/>/g, "<br />");
          desc = desc.replace(/<br>/g, "<br />");
          desc = desc.replace(/(\r\n|\n|\r)/gm, "<br />");
          desc = desc.replace(/<br \/><br \/>/g, "<br />");
          desc = desc.replace(/\\/g, '');
          desc = desc.replace(/"/g, '\\"');

          if (createOrEdit){
            global.client.mutate({
              mutation: gql`mutation{
                addProductWithDetails(dto:{
                  author:"${this.state.author}",
                  longDescription: "${desc}",
                  isbn:"${this.state.isbn}",
                  price:${parseFloat(this.state.price.toString().replace(",", "."))},
                  productName:"${this.state.productName}",
                  publisher: "${this.state.publisher}",
                  publishingDate:"${publishingDate}"
                }){
                  productId
                }
              }`
            }).then(response => {
              this.setState({ productId: response.data.addProductWithDetails.productId, response: "Produkt angelegt." });
              this.props.changeCurrentPage(1);
            }).catch(reason => {
              this.setState({ response: "Produkt konnte nicht angelegt werden" });
            }).then(() => {
              this.setState({ spinner: false });
            });
          } else {
            global.client.mutate({
              mutation: gql`mutation{
                updateProductWithDetails(productId: "${this.props.productId}", dto:{
                  author:"${this.state.author}",
                  longDescription: "${desc}",
                  isbn:"${this.state.isbn}",
                  price:${parseFloat(this.state.price.toString().replace(",", "."))},
                  productName:"${this.state.productName}",
                  publisher: "${this.state.publisher}",
                  publishingDate:"${publishingDate}"
                })
              }`
            }).then(response => {
              this.setState({ response: "Produkt bearbeitet." });
              this.props.changeCurrentPage(1);
            }).catch(reason => {
              this.setState({ response: "Produkt konnte nicht bearbeitet werden" });
            }).then(() => {
              this.setState({ spinner: false });
            });
          }
        } else {
          this.setState({ response: "Das Datum scheint in einem ungewöhnlichem Format zu sein" });
        }
      }
    }
  }

  render() {
    var saveTerm = (this.props.term === "bearbeiten") ? "Speichern & weiter" : "Erstellen & weiter";
    var createEdit = (this.props.term === "bearbeiten") ? false: true;

    return (<div className="modal-content">
      <span id="login-close" className="close" onClick={this.close}>&times;</span>
      <p className="title"><strong>Produkt {this.props.term}</strong></p>

      <div id="createPrPages">
        <p className="active">1. Infos</p>
        <p onClick={() => this.props.changeCurrentPage(1)}>2. Genres</p>
        <p onClick={() => this.props.changeCurrentPage(2)}>3. Bilder</p>
        <p onClick={() => this.props.changeCurrentPage(3)}>4. Cover</p>
      </div>

      <ModalInput title={"Titel"} id={"productName"} value={this.state.productName} onchange={e => this.setState({ productName: e.target.value, edited: true })} class={"half"} />

      <ModalInput title={"Autor"} id={"author"} value={this.state.author} onchange={e => this.setState({ author: e.target.value, edited: true })} class={"half"} />

      <ModalInput title={"Verlag"} id={"publisher"} value={this.state.publisher} onchange={e => this.setState({ publisher: e.target.value, edited: true })} class={"half"} />

      <ModalInput title={"Veröffentlichungsdatum"} id={"publishingDate"} value={this.state.publishingDate} onchange={e => this.setState({ publishingDate: e.target.value, edited: true })} class={"half"} />

      <ModalInput title={"ISBN"} id={"isbn"} value={this.state.isbn} onchange={e => this.setState({ isbn: e.target.value, edited: true })} class={"half"} />

      <ModalInput title={"Preis"} id={"price"} value={this.state.price} onchange={e => this.setState({ price: e.target.value, edited: true })} class={"half"} />

      <p>Beschreibung:</p>
      <textarea id="description" placeholder="Beschreibung" type="text" value={this.state.description} onChange={e => this.setState({ description: e.target.value, edited: true })} />
      <br />
      {this.state.spinner && <div className="loader"></div>}
      <input id="createProduct-submit" type="submit" value={saveTerm} onClick={() => this.createEditProduct(createEdit)} />
      <div id="snackbar">{this.state.response}</div>
    </div>);
  }
}

export default InfoContent;