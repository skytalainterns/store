import React from 'react';
import { Redirect } from 'react-router-dom';
import gql from 'graphql-tag';

import '../css/modal.css';
import '../css/index.css';
import '../css/Loader.css';
import '../css/Snackbar.css';
import '../css/Register.css';

import ModalInput from '../layoutComps/ModalInput.js';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      response: "", admin: false, countryList: [], stateList: [], redirect: false, personalTitle: "", firstName: "", nickname: "", middleName: "", lastName: "", gender: "", birthDate: "", emailAddress: "", userLoginId: "", currentPassword: "", passwordRetype: "", toName: "", address1: "", houseNumberExt: "", city: "", postalCode: "", countryGeoId: "DEU", stateProvinceGeoId: "DE-BY", toggleAddress: false, spinner: false
    };

    this.updateFirstName = this.updateFirstName.bind(this);
    this.updateLastName = this.updateLastName.bind(this);
    this.updateCountryGeoId = this.updateCountryGeoId.bind(this);
    this.updateStateList = this.updateStateList.bind(this);
    this.checkAttribute = this.checkAttribute.bind(this);
    this.timeOut = this.timeOut.bind(this);
  }

  updateStateList(country) {
    global.client.query({
      query: gql`{
        country(geoId:"${country}") {
          regions{
            geoName
            geoId
          }
        }
      }`
    }).then(response => {
      if (response.data !== undefined)
        this.setState({ stateList: response.data.country.regions });
    }).catch(reason => {
      console.error('Login: ', reason);
    }).then(() => { });
  }

  componentDidMount() {
    document.title = "Registierung | Skytala Bookstore";

    if (this.props.countryList !== undefined && this.props.countryList !== null)
      this.setState({ countryList: this.props.countryList });
    this.updateStateList(this.state.countryGeoId);
  }

  updateFirstName(e) {
    this.setState({
      firstName: e.target.value,
      toName: (e.target.value + " " + this.state.lastName)
    });
  }

  updateLastName(e) {
    this.setState({
      lastName: e.target.value,
      toName: (this.state.firstName + " " + e.target.value)
    });
  }

  updateCountryGeoId(e) {
    this.setState({ countryGeoId: e.target.value });
    this.updateStateList(e.target.value);
  }

  isDate(input) {
    var status = false;
    if (!input || input.length <= 0) {
      status = false;
    } else {
      var result = new Date(input);
      if (result.toString() === 'Invalid Date') status = false;
      else status = true;
    }
    return status;
  }

  checkAttribute(v, string) {
    if (v === undefined) this.setState({ response: string + " nicht ausgefüllt." });
    else if (v === null) this.setState({ response: string + " nicht ausgefüllt." });
    else if (v.toString() === "") this.setState({ response: string + " nicht ausgefüllt." });
    else if (v.length === 0) this.setState({ response: string + " nicht ausgefüllt." });
    else if (v.toString() === "null") this.setState({ response: string + " nicht ausgefüllt." });
    else {
      this.setState({ response: "" });
      return true;
    }
    return false;
  }

  submit(e) {
    //checks attributes, then sends register request.
    var okay = true;
    if (!this.checkAttribute(this.state.firstName, "Vorname")) okay = false;
    else if (!this.checkAttribute(this.state.lastName, "Nachname")) okay = false;
    else if (!this.checkAttribute(this.state.gender, "Geschlecht")) okay = false;
    else if (!this.checkAttribute(this.state.birthDate, "Geburtsdatum")) okay = false;
    else if (!this.checkAttribute(this.state.userLoginId, "Benutzername")) okay = false;
    else if (this.state.emailAddress.length === 0 || !this.state.emailAddress.includes("@") || !this.state.emailAddress.includes(".")) {
      this.setState({ response: "E-Mail Adresse muss korrekt ausgefüllt sein." });
      okay = false;
    } else if (this.state.currentPassword.length < 8) {
      this.setState({ response: "Passwort muss mindestens 8 Zeichen lang sein." });
      okay = false;
    } else if (this.state.passwordRetype !== this.state.currentPassword) {
      this.setState({ response: "Passwörter müssen übereinstimmen." });
      okay = false;
    } else if (okay) {
      var date = new Date();

      if (this.state.birthDate.toString().includes(".")) {
        var split = this.state.birthDate.toString().split(".");
        date = new Date(split[2] + "-" + (split[1]) + "-" + split[0]);
      } else {
        date = new Date(this.state.birthDate.toString());
      }

      if (this.isDate(date)) {
        date = date.toISOString();
        this.setState({ spinner: true });

        if (this.state.toggleAddress) {
          if (!this.checkAttribute(this.state.toName, "Name/Firma"))
            okay = false;
          else if (!this.checkAttribute(this.state.address1, "Straße"))
            okay = false;
          else if (!this.checkAttribute(this.state.houseNumberExt, "Hausnummer"))
            okay = false;
          else if (!this.checkAttribute(this.state.city, "Stadt"))
            okay = false;
          else if (!this.checkAttribute(this.state.postalCode, "Postleitzahl"))
            okay = false;
          else if (!this.checkAttribute(this.state.countryGeoId, "Land"))
            okay = false;
          else if (okay) {
            global.client.mutate({
              mutation: gql`mutation {
                registerUserAccount(userDetails: {
                  personalTitle:"${this.state.personalTitle}",
                  firstName: "${this.state.firstName}",
                  nickname: "${this.state.nickname}",
                  middleName: "${this.state.middleName}",
                  lastName: "${this.state.lastName}",
                  gender: ${this.state.gender},
                  birthDate: "${date}",
                  emailAddress: "${this.state.emailAddress}",
                  userLoginId: "${this.state.userLoginId}",
                  currentPassword: "${this.state.currentPassword}",
                  passwordRetype: "${this.state.passwordRetype}",
                  toName: "${this.state.toName}",
                  address1: "${this.state.address1}",
                  houseNumberExt:"${this.state.houseNumberExt}",
                  city: "${this.state.city}",
                  postalCode: "${this.state.postalCode}",
                  countryGeoId: "${this.state.countryGeoId}",
                  stateProvinceGeoId: "${this.state.stateProvinceGeoId}"
                })
              }`
            }).then(response => {
              if (okay) {
                this.setState({ spinner: false, response: "", redirect: true });
                return true;
              } else {
                return false;
              }
            }).catch(reason => {
              this.setState({ response: "Etwas schien nicht richtig zu funktionieren. " + reason, spinner: false });
              return false;
            });
          } else {
            global.client.mutate({
              mutation: gql`mutation {
                registerUserAccount(userDetails: {
                  personalTitle:"${this.state.personalTitle}",
                  firstName: "${this.state.firstName}",
                  nickname: "${this.state.nickname}",
                  middleName: "${this.state.middleName}",
                  lastName: "${this.state.lastName}",
                  gender: ${this.state.gender},
                  birthDate: "${date}",
                  emailAddress: "${this.state.emailAddress}",
                  userLoginId: "${this.state.userLoginId}",
                  currentPassword: "${this.state.currentPassword}",
                  passwordRetype: "${this.state.passwordRetype}"
                })
              }`
            }).then(response => {
              if (okay) {
                this.setState({ spinner: false, response: "", redirect: true });
                return true;
              } else {
                return false;
              }
            }).catch(reason => {
              this.setState({ response: "Etwas schien nicht richtig zu funktionieren. " + reason, spinner: false });
              return false;
            });
          }
        }        
      } else {
        this.setState({ response: "Das Datum scheint in einem unbekannten Format zu sein." });
      }
    }

    return false;
  }

  timeOut(x) {
    x.className = x.className.replace("show", "");
    this.setState({ response: "" });
  }

  render() {
    if (this.state.response.toString().length > 0) {
      var x = document.getElementById("snackbar")
      x.className = "show";
      setTimeout(() => this.timeOut(x), 3000);
    }

    var dropdownCountry = [];
    this.state.countryList.map(function (item, index) {
      return dropdownCountry.push( <option key={index} value={item.geoId}>{item.geoName}</option> );
    }, this);

    var dropdownState = [];
    this.state.stateList.map(function (item, index) {
      return dropdownState.push( <option key={index} value={item.geoId}>{item.geoName}</option> );
    }, this);

    var stateWrap = "";
    if (dropdownState.length > 0) {
      stateWrap = (<div><span>Bundesland/Staat/Provence:</span>
        <select value={this.state.stateProvinceGeoId} onChange={e => this.setState({ stateProvinceGeoId: e.target.value })} required> {dropdownState} </select>
      </div>);
    }

    var pwStrength = "";
    var pwStrClass = 0;
    if (this.state.currentPassword.length < 8) {
      pwStrength = (<span>zu kurz</span>);
      pwStrClass = 0;
    } else if (this.state.currentPassword.length === 8) {
      pwStrength = (<span><br />schwach</span>);
      pwStrClass = 1;
    } else if (this.state.currentPassword.length > 8 && this.state.currentPassword.length <= 10) {
      pwStrength = (<span><br />akzeptabel</span>);
      pwStrClass = 2;
    } else if (this.state.currentPassword.length > 10 && this.state.currentPassword.length <= 12) {
      pwStrength = (<span><br />stark</span>);
      pwStrClass = 3;
    } else if (this.state.currentPassword.length > 12 && this.state.currentPassword.length <= 15) {
      pwStrength = (<span>sehr stark</span>);
      pwStrClass = 4;
    } else if (this.state.currentPassword.length > 15 && this.state.currentPassword.length <= 20) {
      pwStrength = (<span>super stark</span>);
      pwStrClass = 5;
    } else if (this.state.currentPassword.length > 20 && this.state.currentPassword.length <= 50) {
      pwStrength = (<span>über stark</span>);
      pwStrClass = 6;
    } else if (this.state.currentPassword.length > 50) {
      pwStrength = (<span>unmensch-<br />lich stark</span>);
      pwStrClass = 7;
    }

    var pwCompare = "";
    var pwCompClass = "";
    if (this.state.currentPassword.length < 8) {
      pwCompare = (null);
      pwCompClass = "";
    } else if (this.state.currentPassword !== this.state.passwordRetype) {
      pwCompare = (<span>&times;</span>);
      pwCompClass = "nope";
    } else {
      pwCompare = (<span>&#10003;</span>);
      pwCompClass = "okay";
    }

    var toggleAddressClass = (!this.state.toggleAddress) ? "hide" : "";

    return (
      <div id="inner-wrap">
        {this.state.redirect && <Redirect to="/checkYourMails" />}
        <div id="registerContent">
          <p><strong>Registrieren</strong> </p>

          <div className="column">
            <p><strong>Benutzerdetails</strong></p>
            <ModalInput title={"Titel"} id={"personalTitle"} value={this.state.personalTitle} onchange={e => this.setState({ personalTitle: e.target.value })} class={""} />

            <ModalInput title={"Vorname"} id={"firstName"} value={this.state.firstName} onchange={e => this.setState({ firstName: e.target.value })} class={""} />

            <ModalInput title={"Spitzname"} id={"nickname"} value={this.state.nickname} onchange={e => this.setState({ nickname: e.target.value })} class={""} />

            <ModalInput title={"Zweitname"} id={"middleName"} value={this.state.middleName} onchange={e => this.setState({ middleName: e.target.value })} class={""} />

            <ModalInput title={"Nachname"} id={"lastName"} value={this.state.lastName} onchange={e => this.setState({ lastName: e.target.value })} class={""} />

            <span>Geschlecht:</span>
            <select value={this.state.gender} onChange={e => this.setState({ gender: e.target.value })} required>
              <option value=""></option>
              <option value="true">Männlich</option>
              <option value="false">Weiblich</option>
            </select>

            <ModalInput title={"Geburtsdatum"} id={"birthDate"} value={this.state.birthDate} onchange={e => this.setState({ birthDate: e.target.value })} class={""} type={"date"} />

            <ModalInput title={"E-Mail-Adresse"} id={"emailAddress"} value={this.state.emailAddress} onchange={e => this.setState({ emailAddress: e.target.value })} class={""} type={"mail"} />
            <small className="disclaimer">(E-Mail lässt sich im Nachhinein nicht mehr ändern.)</small>
          </div>
          <div className="column">
            <p>
              <strong>Logindetails</strong>
            </p>
            <ModalInput title={"Benutzername"} id={"userLoginId"} value={this.state.userLoginId} onchange={e => this.setState({ userLoginId: e.target.value })} class={""} />
            <small className="disclaimer">(Name lässt sich im Nachhinein nicht mehr ändern.)</small>

            <ModalInput title={"Passwort"} id={"currentPassword"} value={this.state.currentPassword} onchange={e => this.setState({ currentPassword: e.target.value })} class={"pw"} type={"password"} />
            <span id="pwStrength" className={"s" + pwStrClass}>{pwStrength}</span>

            <ModalInput title={"Passwort bestätigen"} id={"passwordRetype"} value={this.state.passwordRetype} onchange={e => this.setState({ passwordRetype: e.target.value })} class={"pw"} type={"password"} />
            <span id="pwComp" className={pwCompClass}>{pwCompare}</span>
          </div>
          <div className={"column address " + toggleAddressClass}>
            <label htmlFor="toggleAddress">
              <input value={this.state.toggleAddress} type="checkbox" name="toggleAddress" id="toggleAddress" onChange={e => this.setState({ toggleAddress: !this.state.toggleAddress })} />
              <p><strong>Standard-Adresse</strong></p>
              <span class="checkmark"></span>
            </label>

            <ModalInput title={"Name / Firma"} id={"toName"} value={this.state.toName} onchange={e => this.setState({ toName: e.target.value })} class={""} />

            <ModalInput title={"Straße"} id={"address1"} value={this.state.address1} onchange={e => this.setState({ address1: e.target.value })} class={"street"} />

            <ModalInput title={""} id={"houseNumberExt"} value={this.state.houseNumberExt} onchange={e => this.setState({ houseNumberExt: e.target.value })} class={"houseNumber"} />

            <ModalInput title={"Postleitzahl"} id={"postalCode"} value={this.state.postalCode} onchange={e => this.setState({ postalCode: e.target.value })} class={""} />

            <ModalInput title={"Stadt"} id={"city"} value={this.state.city} onchange={e => this.setState({ city: e.target.value })} class={""} />

            <span>Land:</span>
            <select value={this.state.countryGeoId} onChange={e => this.updateCountryGeoId(e)}>
              {dropdownCountry}
            </select>

            {stateWrap}
          </div>
        </div>
        <div id="registerSubmitWrap">
          <input id="registerSubmit" type="submit" value="Registieren" onClick={e => this.submit(e)} />
          {this.state.spinner && <div className="loader"></div>}
        </div>
        <div id="snackbar">{this.state.response}</div>
      </div>
    );
  }
}

export default Register;