import React from 'react';

import '../css/index.css';
import '../css/Content.css';
import '../css/Alert.css';

class CheckYourMails extends React.Component {
  render() {
    return (
      <div id="inner-wrap">
        <div className="alert alert-okay">
          <p>
            <strong>Wir haben Ihnen was geschickt.</strong>
            <br /> Überprüfen Sie ihre E-Mails, Sie sollten eine Bestätigungsnachricht erhalten haben.
          </p>
        </div>
      </div>
    );
  }
}

export default CheckYourMails;