import React from 'react';
import ReactDOM from 'react-dom';
import gql from 'graphql-tag';

import '../css/Content.css';
import '../css/DetailView.css';

import EditProduct from './EditProduct';

class DetailView extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: ["loading"], quantity: 1, admin: false, user: "", spinner: false };
    this.editProduct = this.editProduct.bind(this);
    this.submit = this.submit.bind(this);
  }

  //checks user data for adminstate, pulls product information
  componentDidMount() {
    if (this.props.loggedInPerson !== undefined && this.props.loggedInPerson !== null && this.props.loggedInPerson !== "") {
      this.setState({ user: this.props.loggedInPerson.firstName });
      if (this.props.loggedInPerson.authorities.includes("FULLADMIN")) this.setState({ admin: true });
      else this.setState({ admin: false });
    }

    global.client.query({
      query: gql`{
        product(productId:"${this.props.productId}") {
          productAttributes{
            attrValue
            attrName
          }
          productPrices {
            price
          }
          productId
          productName
          description
          longDescription
          mediumImageUrl
          productCategoryMembers {
            productCategory {
              productCategoryId
              categoryName
            }
          }
        }
      }`})
      .then(response => {
        this.setState({ items: response.data.product });
        response.data.product.productAttributes.forEach(function(e) {
          var res = {};
          res[e.attrName] = e.attrValue;
          this.setState(res);
        }, this);
      }).catch(reason => {
        this.setState({items: []});
      });
  }

  //adds a given amount of products to the user cart.
  submit(amount) {
    this.setState({ spinner: true });
    global.client.mutate({
      mutation: gql`mutation {
        addToCart(productId: "${this.props.productId}", count: ${amount})
      }`
    }).then(response => {
      this.setState({ spinner: false });
      if (this.props.refreshItems !== undefined) this.props.refreshItems();
    });
  }

  //renders editProduct modal
  editProduct() {
    ReactDOM.render(
      <EditProduct productId={this.state.items.productId} productName={this.state.items.productName}
        author={this.state.author} publisher={this.state.publisher}
        publishingDate={this.state.publishingDate} isbn={this.state.ISBN}
        description={this.state.items.description} longDescription={this.state.items.longDescription}
        price={this.state.items.productPrices[0].price} mediumImageUrl={this.state.items.mediumImageUrl}
        genres={this.state.items.productCategoryMembers} />, document.getElementById('modal-editProduct'));
    var editPrdkt = document.getElementById('modal-editProduct');
    editPrdkt.style.display = "block";
  }

  render() {
    if (this.state.items.productName !== undefined)
      document.title = this.state.items.productName + " | Skytala Bookstore";
    var title = (this.state.items.productName !== undefined) ? this.state.items.productName : "";

    var description = this.state.items.longDescription;
    if (description === undefined || description == null) {
      description = this.state.items.description;
      if (description === undefined || description == null)
        description = "No Description.";
    }

    var desc = [];
    desc = description.split("<br />");
    for (var i = 0; i < desc.length; i++) {
      desc[i] = <p key={i}>{desc[i]}</p>;
    }

    var img = this.state.items.mediumImageUrl;
    if (img === undefined) {
      var bc = "";
      if (this.state.items.productId !== undefined) {
        for (var ii = 0; ii < this.state.items.productId.length; ii++)
          bc += this.state.items.productId.charCodeAt(ii);
        bc = bc % 16777215;
        bc = bc.toString(16);
      } else bc = "000000";
      img = "http://via.placeholder.com/475x600/" + bc + "/ffffff.png?text=" + title;
    } else img = global.baseURL + img;

    var rating = (this.state.items.productRating !== undefined && this.state.items.productRating !== null) ? this.state.items.productRating : "";

  var ratingStars = (rating !== "") ?(<p id="product-stars" className={"s" + Math.round(rating)}>★{rating.toFixed(2)}</p>) : "";

    var publishingDate = new Date();
    if (this.state.items.publishingDate !== undefined && this.state.items.publishingDate !== null) {
      publishingDate = new Date(this.state.items.publishingDate.toString());
    } else publishingDate = new Date("1970-01-01");
    var dateDifference = (new Date() - publishingDate) / 1000 / 60 / 60 / 24;

    var remark = "";
    if (dateDifference >= 0 && dateDifference <= 60)
      remark = <p id="product-remark" className="newcomer">Newcomer</p>;
    else if (dateDifference < 0) {
      remark = <p id="product-remark" className="preOrder">Vorbestellbar</p>;
    }

    var price = "";
    if (this.state.items.productPrices !== undefined) {
      price = this.state.items.productPrices[0].price;
      price = parseFloat(price.toString().replace(",", ".")).toFixed(2);
      price = price.toString().replace(".", ",");
    }

    var addToCart = (<input id="submit" type="submit" value="Nicht eingeloggt"
      onClick={e => this.submit(this.state.quantity)}
      disabled />);
    if (this.state.user !== "" && this.state.user !== undefined && this.state.user !== null)
      addToCart = (<input id="submit" type="submit" value="Zum Warenkorb hinzufügen"
        onClick={e => this.submit(this.state.quantity)} />);

    if (this.state.items[0] === "loading") {
      return (<div id="inner-wrap">
        <div id="spinner-wrap">
          <div className="loader big"></div>
        </div></div>);
    } else if (this.state.items === "" || this.state.items === undefined || this.state.items === null || this.state.items[0] === "") {
      return (<div id="inner-wrap"><div id="sorry">
        <p><small><i>Dieses Produkt scheint nicht zu existieren.</i></small></p>
      </div></div>);
    } else {
      return (<div id="inner-wrap">
        <div id="response">{this.state.response}</div>
        <div id="product-details">
          <div id="product-image">{ratingStars}{remark}<img src={img} alt="" /></div>
          <div id="product-data">
            <p id="product-title"><strong>{title}</strong></p>
            <p id="product-author">{this.state.author !== undefined && this.state.author}</p>
            <p id="product-publisher">Publisher: <i>{this.state.publisher !== undefined && this.state.publisher}</i></p>
            <p id="product-isbn">{this.state.ISBN !== undefined && ("ISBN " + this.state.ISBN)}|</p>
          </div>
          <div id="product-description">{desc}</div>
          <div id="add-to-cart-wrap">
            {this.state.admin && (<div className="editProductBtnDiv">
              <button id="editProductBtn" onClick={this.editProduct}>Produkt bearbeiten</button>
            </div>)}{addToCart}
            {this.state.spinner && <div className="loader"></div>}
            <input id="detailQuanity" type="text" value={this.state.quantity}
              onChange={e => this.setState({ quantity: e.target.value })} />
            <p className="amount">Anzahl:</p>
            <p id="product-price">{price} €</p>
          </div>
        </div>
      </div>);
    }
  }
}

export default DetailView;
