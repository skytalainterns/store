import React from 'react';
import ReactDOM from 'react-dom';
import gql from 'graphql-tag';

import ECGenre from './ECGenre.js';

class GenreSelectContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      spinner: false
    };

    this.selectGenres = this.selectGenres.bind(this);
    this.updateGenreList = this.updateGenreList.bind(this);
  }

  close() {
    document.getElementById('modal-editProduct').style.display = "none";
    ReactDOM.unmountComponentAtNode(document.getElementById('modal-editProduct'));
  }

  //puts genre into genre list or deletes it from the genre list.
  updateGenreList(genre, selected) {
    let temp = [];
    if (this.state.genres !== undefined && this.state.genres.length > 0 && this.state.genres !== null && this.state.genres !== "") temp = this.state.genres;
    if (selected) temp.push(genre.toString());
    else temp.splice(temp.indexOf(genre), 1);
    this.setState({ genres: temp });
  }

  //sends genreArray with productId to server to change genres(categories).
  selectGenres() {
    this.setState({ response: "", spinner: true });
    var newState = JSON.stringify(this.state.genres);

    if (newState.length === 0) {
      this.setState({ currentPage: 2 });
    } else {
      this.state.genres.forEach(function(genre) {
        global.client.mutate({
          mutation: gql`mutation {
            addProductToCategories(productId:"${this.props.productId}",categoryIds:${newState}){
              body
              contentType
              status
            }
          }`
        }).then(response => {
          this.setState({ response: "Genres geändert." });
          this.props.changeCurrentPage(2);
        }).catch(reason => {
          this.setState({ response: "Genres konnte nicht geändert werden." });
        }).then(() => {
          this.setState({ spinner: false });
        });
      }, this);
    }
  }

  render() {
    var saveTerm = (this.props.term === "bearbeiten") ? "Speichern & Weiter" : "Auswählen & Weiter"; 
    var updateGenreList = this.updateGenreList.bind(this);
    var receivedGenres = this.props.genres;

    return (<div className="modal-content">
      <span id="login-close" className="close" onClick={this.close}>&times;</span>
      <p className="title"><strong>Produkt bearbeiten</strong></p>

      <div id="createPrPages">
        <p onClick={() => this.props.changeCurrentPage(0)}>1. Infos</p>
        <p className="active">2. Genres</p>
        <p onClick={() => this.props.changeCurrentPage(2)}>3. Bilder</p>
        <p onClick={() => this.props.changeCurrentPage(3)}>4. Cover</p>
      </div>

      {this.props.genreList.map(function (item, index) {
        return <ECGenre key={index} genre={item} updateGenreList={updateGenreList} receivedGenres={receivedGenres} />;
      })}

      {this.state.spinner && <div className="loader"></div>}
      <input id="createProduct-submit" type="submit" value={saveTerm} onClick={this.selectGenres} />
      <div id="snackbar">{this.state.response}</div>
    </div>);
  }
}

export default GenreSelectContent;