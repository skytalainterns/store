import React from 'react';
import { Redirect } from 'react-router-dom';
import gql from 'graphql-tag';

import '../css/Content.css';
import '../css/OrderView.css';
import '../css/Loader.css';

import ProductList from '../product/ProductList';

class OrderDetailView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderId: "loading", orderDate: "", statusId: "", toName: "", address1: "", houseNumberExt: "", city: "", postalCode: "", countryGeoId: "", stateProvinceGeoId: "", products: [], grandTotal: "", response: "", redirect: false
    };

    this.cancel = this.cancel.bind(this);
  }

  componentDidMount() {
    /*
    tried to use [the following] to get the shipment date for the order, but something seems to be wrong in the back-end
    shipments {
      destinationContactMech {
        toName
        address1
        houseNumberExt
        city
        postalCode
        countyGeoId
        stateProvinceGeoId
      }
    }
    */


    global.client.query({
      query: gql`{
        order(orderId:"${this.props.orderId}"){
          orderId
          grandTotal
          statusId
          currencyUom
          orderDate

          orderItems{
            quantity
            product{
              productName
              productId
              mediumImageUrl
              productAttributes{
                attrName
                attrValue
              }
              productPrices{
                price
              }
            }
          }
        }
      }`
    }).then(response => {
      if (response.data !== '') {
        this.setState({
          orderId: response.data.order.orderId,
          orderDate: response.data.order.orderDate,
          statusId: response.data.order.statusId,
          toName: response.data.order.toName,
          address1: response.data.order.address1,
          houseNumberExt: response.data.order.houseNumberExt,
          city: response.data.order.city,
          postalCode: response.data.order.postalCode,
          countryGeoId: response.data.order.countryGeoId,
          stateProvinceGeoId: response.data.order.stateProvinceGeoId,
          grandTotal: response.data.order.grandTotal
        });

        var products = [];

        var tempPrs = response.data.order.orderItems;
        tempPrs.forEach(function (pr) {
          let product = {
            productName: "",
            productPrices: "",
            productId: "",
            author: "",
            publishingDate: "",
            publisher: "",
            ISBN: "",
            mediumImageUrl: ""
          };

          //re-write productAttrbute array to obj-attributes so they can be used better.
          product.productId = pr.product.productId;
          product.productId = pr.product.productId;
          product.productName = pr.product.productName;
          product.productPrices = pr.product.productPrices;
          product.mediumImageUrl = pr.product.mediumImageUrl;
          product.author = pr.product.author;
          product.publishingDate = pr.product.publishingDate;
          product.publisher = pr.product.publisher;
          product.ISBN = pr.product.ISBN;
          product.quantity = pr.quantity;

          pr.product.productAttributes.forEach(function (attr) { product[attr.attrName] = attr.attrValue; }, this);
          products.push(product);
        }, this);

        this.setState({ products: products });
      }
    });
  }

  cancel() {
    global.client.mutate({
      mutation: gql`mutation{
        cancelOrder(orderId: "${this.props.orderId}") {
          statusId
        }
      }`
    }).then(response => {
      response = response.data;
      if (response !== '') { this.setState({ response: response, redirect: true }); }
    });
  }

  render() {
    if (this.state.orderId !== "" && this.state.orderId !== "loading" && this.state.orderId !== undefined) {
      var totalItems = 0;
      this.state.products.forEach(item => totalItems += parseInt(item.quantity, 10));

      var shipDate = new Date(parseInt(this.state.orderDate, 10));
      var shipDateString = ((shipDate.getDate() < 10) ? ("0" + shipDate.getDate()) + "." : shipDate.getDate() + ".") + (((shipDate.getMonth() + 1) < 10) ? ("0" + (shipDate.getMonth() + 1) + ".") : (shipDate.getMonth() + 1) + ".") + shipDate.getFullYear() + " " + ((shipDate.getHours() < 10) ? ("0" + shipDate.getHours()) + ":" : shipDate.getHours() + ":") + ((shipDate.getMinutes() < 10) ? ("0" + shipDate.getMinutes()) : shipDate.getMinutes());

      var cancelBtn = <button onClick={this.cancel}>Bestellung stornieren</button>;
      if (this.state.statusId !== "ORDER_PROCESSING" && this.state.statusId !== "ORDER_HOLD" && this.state.statusId !== "ORDER_APPROVED")
        cancelBtn = <button onClick={this.cancel} disabled>Bestellung stornieren</button>;

      var colClass = ["col", "col", "col", "col", "col sep", "col", "col", "col"];

      switch (this.state.statusId) {
        case "ORDER_PROCESSING": colClass[0] = "col active1"; break;
        case "ORDER_APPROVED": colClass[1] = "col active2"; break;
        case "ORDER_CREATED": colClass[2] = "col active3"; break;
        case "ORDER_SENT": colClass[3] = "col active4"; break;
        case "ORDER_COMPLETED": colClass[4] = "col sep active5"; break;
        case "ORDER_CANCELLED": colClass[5] = "col active6"; break;
        case "ORDER_HOLD": colClass[6] = "col active7"; break;
        case "ORDER_REJECTED": colClass[7] = "col active8"; break;
        default: break;
      }

      return (
        <div id="inner-wrap">
          {this.state.redirect && <Redirect to="/order" />}
          <div id="orderDetails">
            <div id="generalData">
              <div className="colDetail">
                <p>Bestellung #{this.state.orderId}</p>
                <p>Bestellwert: {this.state.grandTotal.toString().replace(".", ",")} €</p>
                <p>Bestellgröße: {totalItems} Artikel</p>
                <p>Bestelldatum: {shipDateString}</p>
              </div>

              <div className="colDetail">
                <p>Name: {this.state.toName}</p>
                <p>Adresse: {this.state.address1} {this.state.houseNumberExt}</p>
                <p>Stadt: {this.state.city}</p>
                <p>Postleitzahl: {this.state.postalCode}</p>
                <p>Bundesland: {this.state.stateProvinceGeoId}</p>
                <p>Land: {this.state.countryGeoId}</p>
              </div>

              <div className="remove"> {cancelBtn} </div>

              <p>Status: </p>
              <div id="stateDetail">
                <div className={colClass[0]}> <p>Verarbeitung</p> </div>
                <div className={colClass[1]}> <p>Genehmigt</p> </div>
                <div className={colClass[2]}> <p>Bezahlt</p> </div>
                <div className={colClass[3]}> <p>Versandt</p> </div>
                <div className={colClass[4]}> <p>Abgeschlossen</p> </div>
                <div className={colClass[5]}> <p>Storniert</p> </div>
                <div className={colClass[6]}> <p>Pausiert</p> </div>
                <div className={colClass[7]}> <p>Abgelehnt</p> </div>
              </div>
            </div>

            <ProductList items={this.state.products} />
          </div>
        </div>
      );
    } else if (this.state.orderId === "loading") {
      return (<div id="inner-wrap">
        <div id="spinner-wrap"><div className="loader big"></div></div>
      </div>);
    } else {
      return (<div id="inner-wrap"><div id="sorry">
        <p><small><i>Diese Bestellung scheint nicht zu existieren.</i></small></p>
      </div></div>);
    }
  }
}

export default OrderDetailView;