import React from 'react';

class ECGenre extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      genresID: this.props.genre.productCategoryId,
      genresName: this.props.genre.categoryName,
      spinner: false,
      edited: false,
      selected: false
    };

    this.toggleGenre = this.toggleGenre.bind(this);
  }

  //selects or deselects genre
  toggleGenre(e) {
    this.props.updateGenreList(this.state.genresID, !this.state.selected);
    this.setState({ selected: !this.state.selected });
  }

  componentDidMount() {
    this.setState({ genresID: this.props.genre.productCategoryId, genresName: this.props.genre.categoryName });

    this.props.receivedGenres.forEach(function (element) {
      if (element.productCategory.productCategoryId === this.props.genre.productCategoryId)
        this.setState({ selected: true });
    }, this);
  }

  render() {
    return (
      <div className="genreCheckbox">
        <label htmlFor={this.state.genresID}>
          <input checked={this.state.selected} type="checkbox" name={this.state.genresID} id={this.state.genresID} onChange={e => this.toggleGenre(e)} />
          <p>{this.state.genresName}</p>
          <span className="checkmark"></span>
        </label>
      </div>
    );
  }
}

export default ECGenre;