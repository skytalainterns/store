import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import ApolloClient from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import './css/index.css';
import './css/modal.css';
import './css/DetailView.css';

import AppWrap from './AppWrap';

const superagent = require('superagent');
const superagentAbsolute = require('superagent-absolute');

global.baseURL = 'https://192.168.49.59:8443';
global.request = superagentAbsolute(superagent.agent())(global.baseURL);
global.client = new ApolloClient({
	link: new HttpLink({
		uri: 'http://192.168.49.55:5000/',
		credentials: 'include'
	}),
	cache: new InMemoryCache()
});

ReactDOM.render((<AppWrap/>), document.getElementById('root'));
registerServiceWorker();
export var admin;