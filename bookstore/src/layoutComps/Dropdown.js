import React from 'react';
import { Link } from 'react-router-dom';

class Dropdown extends React.Component {
  render() {
    //renders given links and buttons in drop down
    return (
      <div id="account" className="dropdown">
        <button className="dropbtn" onClick={this.props.onclick}><span>{this.props.login}</span></button>
        <div className="dropdown-content" id="dropdown-account">
          {this.props.content.map(link => {
            if(link.type === "Link"){
              return (<Link to={link.to}>{link.badge !== undefined && link.badge} {link.text}</Link>);
            } else if(link.type === "span"){
              return (<span id={link.id} onClick={link.onclick}>{link.text}</span>);
            }
            return (null);
          })}
        </div>
      </div>
    );
  }
}

export default Dropdown;